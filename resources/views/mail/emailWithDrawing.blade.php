@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $string['write_email'] }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('site.email.send') }}">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ $string['your_email_address'] }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row" disabled>
                                <label for="subject" class="col-md-4 col-form-label text-md-right">{{ $string['subject'] }}</label>

                                <div class="col-md-6">
                                    <input id="subject" type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" value="'{{$drawing->title}}' - {{ $drawing->getArtist->name }}" required autofocus>

                                    @if ($errors->has('subject'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="message" class="col-md-4 col-form-label text-md-right">{{ $string['message'] }}</label>

                                <div class="col-md-6">
                                    <textarea id="message" type="text" rows='7' class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" value="{{ old('message') }}" required></textarea>

                                    @if ($errors->has('message'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="captcha" class="col-md-4 col-form-label text-md-right">{{ $string['captcha'] }}</label>

                                <div class="col-md-6">
                                    {!! captcha_img() !!}
                                    <input id="captcha" type="text" class="form-control{{ $errors->has('captcha') ? ' is-invalid' : '' }}" name="captcha" required/>

                                    @if ($errors->has('captcha'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('captcha') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ $string['send'] }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection