@extends('layouts.app')

@section('meta_tags')
    <title>
        Association of Mediterranean Artists. Cyprus online art gallery
    </title>
@endsection

@section('content')

    <div class="container">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel mt-md-5">
            <div class="container">
                <ul class="nav navbar-nav row">
                    <li class="nav-item">
                        <button class="btn btn-link nav-link ml-md-3">@sortablelink('title', $string['sortbytitle'])</button>
                    </li>
                    <li class="nav-item">
                        <button class="btn btn-link nav-link ml-md-3">@sortablelink('price', $string['sortbyprice'])</button>
                    </li>
                    <li class="nav-item">
                        <button class="btn btn-link nav-link ml-md-3">@sortablelink('updated_at', $string['sortbynewness'])</button>
                    </li>
                    <li class="nav-item">
                        <button class="btn btn-link nav-link ml-md-3">@sortablelink('genre', $string['genre'])</button>
                    </li>
                </ul>
                <form class="form-inline pull-xs-right" action="{{route('drawing.find')}}" method="GET">
                    <div class="form-group">
                        <input id="search" type="text"
                               class="form-control{{ $errors->has('search') ? ' is-invalid' : '' }}" name="search"
                               placeholder="{{ $string['search_by_name'] }}" required autofocus @if(isset($term)) value="{{ $term }}" @endif>

                        @if ($errors->has('search'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('search') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-outline-success ml-md-2">
                        {{ $string['search'] }}
                    </button>
                </form>
            </div>
        </nav>
        <div class="masonry-grid">
            @foreach($drawings as $drawing)
                @if($drawing->activated || (Auth::check() && Auth::user()->admin))
                    <div class="col-md-4 masonry-item">
                        <div class="hovereffect">
                            <div class="thumbnail">
                                <img src="{{ asset($drawing->picture) }}" alt="">
                            </div>
                            <div class="overlay">
                                <h1><a class="col-md-4"
                                       href="{{ route('drawing.single',['id'=>$drawing->id]) }}">{{$drawing->title}}</a>
                                </h1>

                                    <a href="{{ route('artist.single',['id'=>$drawing->artistId]) }}">{{ $string['author']}}
                                        : {{\App\User::find($drawing->artistId)->name}}</a>

                                <p>{{ $string['size'] }}: {{$drawing->size}}</p>
                                <h1>{{ $string['price'] }}: {{$drawing->price}}€</h1>
                                @if(!$drawing->activated)
                                    <h1>Not activated</h1>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        {!! $drawings->appends(\Request::except('page'))->links() !!}
    </div>
@endsection

@section('js_includes')
    <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <script>
        $( ".thumbnail" ).each(function(  ) {
            var image_height = $( this ).find('img').height();
            var image_width = $( this ).find('img').width();
            if(image_height/image_width <= 1)
            {
                $(this).find('img').addClass('landscape');
            }
            else
            {
                $(this).find('img').addClass('portrait');
            }
        });

        $('.thumbnail').css("min-height", 260);


        $(document).ready(function() {
            var $container = $('.masonry-grid');
            $container.imagesLoaded(function () {
                $container.masonry({
                    itemSelector: '.masonry-item',
                    columnWidth: 0
                });
            });
        });

    </script>
@endsection