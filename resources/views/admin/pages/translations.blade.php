@extends('admin.layout.app')

@section('breadcrumbs')
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Translations</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Translations grid</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="content mt-3">

        <div class="col-sm-12 col-lg-12">
            <div class="card">

                <table class="table">
                    <form action="{{ route('admin.translations') }}" method="GET">
                        <thead>
                        <tr>
                            <th scope="col">Search by code or translation</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col">Search</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row"><input type="text" name="search" placeholder="Code or translation" @if(isset($term)) value="{{ $term }}" @endif></th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><input type="submit" value="Search"></td>
                        </tr>
                        </tbody>
                    </form>
                </table>

                <table class="table">
                    <form action="{{ route('admin.translation.create') }}" method="POST">
                        {{ csrf_field() }}
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Code</th>
                            <th scope="col">Translated string</th>
                            <th scope="col">Language</th>
                            <th scope="col">Edit page</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Create new translation: </th>
                                <td><input type="text" name="code" placeholder="Code"></td>
                                <td><input type="text" name="content" placeholder="Translated string"></td>
                                <td>
                                    <select name="language_id" id="">
                                        @foreach($languages as $language)
                                            <option value="{{ $language->id }}">
                                                {{ $language->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input type="submit" value="Create"></td>
                            </tr>
                        </tbody>
                    </form>
                </table>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Code</th>
                        <th scope="col">Translated string</th>
                        <th scope="col">Language</th>
                        <th scope="col">Delete</th>
                        <th scope="col">Edit page</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($translations as $translation)
                            <tr>
                                <th scope="row">{{ $translation->id }}</th>
                                <td>{{ $translation->code }}</td>
                                <td>{{ limit_text($translation->content,100) }}</td>
                                <td>{{ $translation->language->title }}</td>
                                <td><a href="{{ route('admin.translation.remove',['id'=>$translation->id]) }}">Delete</a></td>
                                <td><a href="{{ route('admin.translation.update.form',['id'=>$translation->id]) }}">Edit translation {{ $translation->code }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $translations->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
        <!--/.col-->

    </div> <!-- .content -->
@endsection