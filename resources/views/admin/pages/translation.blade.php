@extends('admin.layout.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('admin.translation.update') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" name="translation_id" value="{{ $translation->id }}">
                <div class="card-header">
                    <strong>{{ $translation->code }}</strong> Updating Form
                </div>
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Translation code</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="code" placeholder="Translation code" value="{{ $translation->code }}" class="form-control"><small class="form-text text-muted">Type without scape symbol like "example_translation_code"</small></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Translated string</label></div>
                        <div class="col-12 col-md-9">
                            <textarea id="text-input" name="content" placeholder="Translated string" class="form-control">{{ $translation->content }}</textarea>
                            <small class="form-text text-muted">Type without scape symbol like "example_translation_code"</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label">Choose translation language</label></div>
                        <div class="col col-md-9">
                            <div class="form-check">
                                @foreach($languages as $language)
                                    <div class="radio">
                                        <label for="radio-role-{{ $language->id }}" class="form-check-label ">
                                            <input type="radio" id="radio-role-{{ $language->id }}" name="language_id" value="{{ $language->id }}" @if($translation->language != null && $translation->language->id == $language->id) checked @endif class="form-check-input">{{ $language->title }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>

                    <button type="reset" class="btn btn-danger btn-sm" onclick="location.href='{{ URL::previous() }}';">
                        <i class="fa fa-ban"></i> Reset
                    </button>

                </div>
            </form>
        </div>
    </div>
@endsection