@extends('admin.layout.app')

@section('breadcrumbs')
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Roles</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Roles grid</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="content mt-3">

        <div class="col-sm-12 col-lg-12">
            <div class="card">
                <table class="table">
                    <form action="{{ route('admin.role.create') }}" method="POST">
                        {{ csrf_field() }}
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Show in menagement</th>
                            <th scope="col">Show in artists</th>
                            <th scope="col">Edit page</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Create new role: </th>
                                <td><input type="text" name="title" placeholder="Title"></td>
                                <td><input type="checkbox" name="show_in_management" value="1"></td>
                                <td><input type="checkbox" name="show_in_artists" value="1"></td>
                                <td><input type="submit" value="Create"></td>
                            </tr>
                        </tbody>
                    </form>
                </table>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Show in menagement</th>
                        <th scope="col">Show in artists</th>
                        <th scope="col">Edit page</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <th scope="row">{{ $role->id }}</th>
                                <td>{{ $role->title }}</td>
                                <td>{{ $role->show_in_management }}</td>
                                <td>{{ $role->show_in_artists }}</td>
                                <td><a href="{{ route('admin.role.update.form',['id'=>$role->id]) }}">Edit role {{ $role->title }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $roles->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
        <!--/.col-->

    </div> <!-- .content -->
@endsection