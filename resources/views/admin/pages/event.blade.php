@extends('admin.layout.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('admin.event.update') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" name="event_id" value="{{ $event->id }}">
                <div class="card-header">
                    <strong>{{ $event->name }}</strong> Updating Form
                </div>
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Name</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-input" name="name" placeholder="Title" value="{{ $event->name }}" class="form-control">
                            <small class="form-text text-muted">Type event name</small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">City</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-input" name="city" placeholder="City" value="{{ $event->city }}" class="form-control">
                            <small class="form-text text-muted">City</small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Country</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-input" name="country" placeholder="Country" value="{{ $event->country }}" class="form-control">
                            <small class="form-text text-muted">Country</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Date of start</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="date" id="text-input" name="date_start" placeholder="Date of start" value="{{ $event->date_start }}" class="form-control">
                            <small class="form-text text-muted">Date of start</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Date of fin</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="date" id="text-input" name="date_fin" placeholder="Date of fin" value="{{ $event->date_fin }}" class="form-control">
                            <small class="form-text text-muted">Date of fin</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Activated</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="checkbox" name="activated"  @if($event->activated) value="1" checked @endif class="form-control">
                            <small class="form-text text-muted">Activated</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Type</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-input" name="type" placeholder="Type" value="{{ $event->type }}" class="form-control" maxlength="255">
                            <small class="form-text text-muted">Type</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Description</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <textarea id="text-input" placeholder="Description" name="description" class="form-control" maxlength="255">{{ $event->description }}</textarea>
                            <small class="form-text text-muted">Description</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Link</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-input" name="link" placeholder="Link" value="{{ $event->link }}" class="form-control" maxlength="255">
                            <small class="form-text text-muted">Link</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="file-input" class=" form-control-label">Image</label></div>
                        <div class="col-12 col-md-9">
                            @if($event->image == null)
                                <p>This event has no picture yet</p>
                            @else
                                <img src="{{ asset($event->image) }}" alt="" width="400px">
                            @endif
                            <input id="image" type="file" class="form-control" name="image">
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>

                    <button type="reset" class="btn btn-danger btn-sm" onclick="location.href='{{ URL::previous() }}';">
                        <i class="fa fa-ban"></i> Reset
                    </button>

                </div>
            </form>
        </div>
    </div>
@endsection