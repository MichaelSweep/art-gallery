@extends('admin.layout.app')

@section('breadcrumbs')
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Drawings</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Drawings grid</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="content mt-3">

        <div class="col-sm-12 col-lg-12">
            <div class="card">

                <table class="table">
                    <form action="{{ route('admin.drawings') }}" method="GET">
                        <thead>
                        <tr>
                            <th scope="col">Search</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col">Search</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row"><input type="text" name="search" placeholder="Type your term" @if(isset($term)) value="{{ $term }}" @endif></th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><input type="submit" value="Search"></td>
                        </tr>
                        </tbody>
                    </form>
                </table>

                <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="{{ route('admin.drawing.create.form') }}">Create drawing</a></td>
                            </tr>
                        </tbody>

                </table>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">City</th>
                        <th scope="col">Country</th>
                        <th scope="col">Date</th>
                        <th scope="col">Description</th>
                        <th scope="col">Genre</th>
                        <th scope="col">Painting media</th>
                        <th scope="col">Size</th>
                        <th scope="col">Status</th>
                        <th scope="col">Price</th>
                        <th scope="col">Artist</th>
                        <th scope="col">Activated</th>
                        <th scope="col">Edit page</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($drawings as $drawing)
                            <tr>
                                <th scope="row">{{ $drawing->id }}</th>
                                <td>{{ $drawing->title }}</td>
                                <td>{{ $drawing->city }}</td>
                                <td>{{ $drawing->country }}</td>
                                <td>{{ $drawing->date }}</td>
                                <td>{{ limit_text($drawing->description, 400) }}</td>
                                <td>{{ $drawing->genre }}</td>
                                <td>{{ $drawing->technology }}</td>
                                <td>{{ $drawing->size }}</td>
                                <td>{{ $drawing->status }}</td>
                                <td>{{ $drawing->price }}</td>
                                <td>{{ $drawing->getArtist->name }}</td>
                                <td>{{ $drawing->activated }}</td>
                                <td><a href="{{ route('admin.drawing.update.form',['id'=>$drawing->id]) }}">Edit drawing {{ $drawing->title }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $drawings->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
        <!--/.col-->

    </div> <!-- .content -->
@endsection