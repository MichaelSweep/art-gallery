@extends('admin.layout.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <form action="{{route('admin.drawing.create')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-header">
                    <strong>Drawing</strong> Creating Form
                </div>
                <div class="form-group row">
                    <label for="title" class="col-md-4 col-form-label text-md-right">{{ $string['title'] }}</label>

                    <div class="col-md-6">
                        <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>

                        @if ($errors->has('title'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="city" class="col-md-4 col-form-label text-md-right">{{ $string['city'] }}</label>

                    <div class="col-md-6">
                        <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}">

                        @if ($errors->has('city'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="country" class="col-md-4 col-form-label text-md-right">{{ $string['country'] }}</label>

                    <div class="col-md-6">
                        <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}">

                        @if ($errors->has('country'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="date" class="col-md-4 col-form-label text-md-right">{{ $string['date'] }}</label>

                    <div class="col-md-6">
                        <input id="date" type="date" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" value="{{ old('date') }}">

                        @if ($errors->has('date'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-md-4 col-form-label text-md-right">{{ $string['description'] }}</label>

                    <div class="col-md-6">
                        <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" maxlength="300">

                        @if($errors->has('description'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="genre" class="col-md-4 col-form-label text-md-right">{{ $string['genre'] }}</label>

                    <div class="col-md-6">
                        <select name="genre" class="form-control">
                            <option value="Landscape">Landscape</option>
                            <option value="Portrait">Portrait</option>
                            <option value="Still life">Still life</option>
                            <option value="Religious">Religious</option>
                            <option value="Nude">Nude</option>
                            <option value="Abstract">Abstract</option>
                            <option value="Poster">Poster</option>
                        </select>
                        @if ($errors->has('genre'))
                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('genre') }}</strong>
                                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="technology" class="col-md-4 col-form-label text-md-right">{{ $string['technology'] }}</label>

                    <div class="col-md-6">

                        <select name="technology" class="form-control">
                            <option value="Oil on canvas">Oil on canvas</option>
                            <option value="Oil on paper">Oil on paper</option>
                            <option value="Pastel">Pastel</option>
                            <option value="Acrylic">Acrylic</option>
                            <option value="Pencil">Pencil</option>
                            <option value="Watercolor">Watercolor</option>
                            <option value="Ink">Ink</option>
                            <option value="Batik">Batik</option>
                            <option value="Fresco">Fresco</option>
                            <option value="Gouache">Gouache</option>
                            <option value="Enamel">Enamel</option>
                            <option value="Spray paint">Spray paint</option>
                            <option value="Tempera">Tempera</option>
                            <option value="Digital painting">Digital painting</option>
                            <option value="Photo">Photo</option>
                        </select>

                        @if ($errors->has('technology'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('technology') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="size" class="col-md-4 col-form-label text-md-right">{{ $string['size'] }}</label>

                    <div class="row ml-6">
                        <input id="sizeOne" type="number" class="form-control col-md-2 mr-1{{ $errors->has('sizeOne') ? ' is-invalid' : '' }}" name="sizeOne" style="margin-left:25px;">
                        x
                        <input id="sizeTwo" type="number" class="form-control col-md-2 ml-1{{ $errors->has('sizeTwo') ? ' is-invalid' : '' }}" name="sizeTwo">
                        <select class="ml-1" name="unit">
                            <option value="Cm">Cm</option>
                            <option value="Dm">Dm</option>
                        </select>
                        @if ($errors->has('size'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('size') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="status" class="col-md-4 col-form-label text-md-right">{{ $string['status'] }}</label>

                    <div class="col-md-6">
                        <select name="status" class="form-control">
                            <option value="For Sale">{{ $string['for_sale'] }}</option>
                            <option value="Sold">{{ $string['sold'] }}</option>
                            <option value="Not for Sale">{{ $string['not_for_sale'] }}</option>
                            <option value="Private Collection">{{ $string['private_collection'] }}</option>
                            <option value="Donated">{{ $string['donated'] }}</option>
                            <option value="Charity sale">{{ $string['charity sale'] }}</option>
                            <option value="Artists collection">{{ $string['artists collection'] }}</option>
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right">{{ $string['price'] }}</label>

                    <div class="col-md-3">
                        <input id="price" type="number" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price">

                        @if ($errors->has('price'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="col" style="position: relative;">€</div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right">Artist id</label>

                    <div class="col-md-3">
                        <input id="artistId" type="number" class="form-control{{ $errors->has('artistId') ? ' is-invalid' : '' }}" name="artistId">

                        @if ($errors->has('artistId'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('artistId') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div>
                        Please search for the artist id (sign #) on the <a href="{{ route('admin.artists') }}">artists page</a> Your id is "{{ Auth::id() }}"
                    </div>
                </div>

                <div class="form-group row">
                    <label for="image" class="col-md-4 col-form-label text-md-right">{{ $string['image'] }}</label>

                    <div class="col-md-6">
                        <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image">

                        @if ($errors->has('image'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ $string['submit'] }}
                        </button>
                    </div>
                </div>


            </form>
        </div>
    </div>
@endsection