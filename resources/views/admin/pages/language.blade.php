@extends('admin.layout.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('admin.language.update') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" name="language_id" value="{{ $language->id }}">
                <div class="card-header">
                    <strong>{{ $language->title }}</strong> Updating Form
                </div>
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Language title</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="title" placeholder="Language title" value="{{ $language->title }}" class="form-control"><small class="form-text text-muted">Language title</small></div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>

                    <button type="reset" class="btn btn-danger btn-sm" onclick="location.href='{{ URL::previous() }}';">
                        <i class="fa fa-ban"></i> Reset
                    </button>

                </div>
            </form>
        </div>
    </div>
@endsection