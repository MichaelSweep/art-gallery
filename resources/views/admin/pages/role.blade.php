@extends('admin.layout.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('admin.role.update') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" name="role_id" value="{{ $role->id }}">
                <div class="card-header">
                    <strong>{{ $role->title }}</strong> Updating Form
                </div>
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Role title</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="title" placeholder="Artist name" value="{{ $role->title }}" class="form-control"><small class="form-text text-muted">Role title</small></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label">Show in management page</label></div>
                        <div class="col col-md-9">
                            <div class="form-check">
                                <div class="radio">
                                    <label for="radio-show-in-management-1" class="form-check-label ">
                                        <input type="radio" id="radio-show-in-management-1" name="show_in_management" value="1" @if($role->show_in_management) checked @endif class="form-check-input">Show
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="radio-show-in-management-2" class="form-check-label ">
                                        <input type="radio" id="radio-show-in-management-2" name="show_in_management" value="0" @if(!$role->show_in_management) checked @endif class="form-check-input">Don't show
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label">Show in artists page</label></div>
                        <div class="col col-md-9">
                            <div class="form-check">
                                <div class="radio">
                                    <label for="radio-show-in-artists-1" class="form-check-label ">
                                        <input type="radio" id="radio-show-in-artists-1" name="show_in_artists" value="1" @if($role->show_in_artists) checked @endif class="form-check-input">Show
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="radio-show-in-artists-0" class="form-check-label ">
                                        <input type="radio" id="radio-show-in-artists-0" name="show_in_artists" value="0" @if(!$role->show_in_artists) checked @endif class="form-check-input">Don't show
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>

                    <button type="reset" class="btn btn-danger btn-sm" onclick="location.href='{{ URL::previous() }}';">
                        <i class="fa fa-ban"></i> Reset
                    </button>

                </div>
            </form>
        </div>
    </div>
@endsection