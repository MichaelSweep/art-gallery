@extends('admin.layout.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('admin.drawing.update') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" name="drawing_id" value="{{ $drawing->id }}">
                <div class="card-header">
                    <strong>{{ $drawing->title }}</strong> Updating Form
                </div>
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Title</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-input" name="title" placeholder="Title" value="{{ $drawing->title }}" class="form-control" maxlength="120">
                            <small class="form-text text-muted">Type your title</small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">City</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-input" name="city" placeholder="City" value="{{ $drawing->city }}" class="form-control">
                            <small class="form-text text-muted">City</small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Country</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-input" name="country" placeholder="Country" value="{{ $drawing->country }}" class="form-control">
                            <small class="form-text text-muted">Country</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Date</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="date" id="text-input" name="date" placeholder="Date" value="{{ $drawing->date }}" class="form-control">
                            <small class="form-text text-muted">Date</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Description</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <textarea id="text-input" name="description" placeholder="Description" class="form-control" maxlength="1000">{{ $drawing->description }}</textarea>
                            <small class="form-text text-muted">Description</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Genre</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="genre" class="form-control">
                                <option @if($drawing->genre == 'Landscape') selected @endif value="Landscape">Landscape</option>
                                <option @if($drawing->genre == 'Portrait') selected @endif value="Portrait">Portrait</option>
                                <option @if($drawing->genre == 'Still life') selected @endif value="Still life">Still life</option>
                                <option @if($drawing->genre == 'Religious') selected @endif value="Religious">Religious</option>
                                <option @if($drawing->genre == 'Nude') selected @endif value="Nude">Nude</option>
                                <option @if($drawing->genre == 'Abstract') selected @endif value="Abstract">Abstract</option>
                                <option @if($drawing->genre == 'Poster') selected @endif value="Poster">Poster</option>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Painting media</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="technology" class="form-control">
                                <option @if($drawing->technology == 'Oil on canvas') selected @endif value="Oil on canvas">Oil on canvas</option>
                                <option @if($drawing->technology == 'Oil on paper') selected @endif value="Oil on paper">Oil on paper</option>
                                <option @if($drawing->technology == 'Pastel') selected @endif value="Pastel">Pastel</option>
                                <option @if($drawing->technology == 'Acrylic') selected @endif value="Acrylic">Acrylic</option>
                                <option @if($drawing->technology == 'Pencil') selected @endif value="Pencil">Pencil</option>
                                <option @if($drawing->technology == 'Watercolor') selected @endif value="Watercolor">Watercolor</option>
                                <option @if($drawing->technology == 'Ink') selected @endif value="Ink">Ink</option>
                                <option @if($drawing->technology == 'Batik') selected @endif value="Batik">Batik</option>
                                <option @if($drawing->technology == 'Fresco') selected @endif value="Fresco">Fresco</option>
                                <option @if($drawing->technology == 'Gouache') selected @endif value="Gouache">Gouache</option>
                                <option @if($drawing->technology == 'Enamel') selected @endif value="Enamel">Enamel</option>
                                <option @if($drawing->technology == 'Spray paint') selected @endif value="Spray paint">Spray paint</option>
                                <option @if($drawing->technology == 'Tempera') selected @endif value="Tempera">Tempera</option>
                                <option @if($drawing->technology == 'Digital painting') selected @endif value="Digital painting">Digital painting</option>
                                <option @if($drawing->technology == 'Photo') selected @endif value="Photo">Photo</option>
                                <option @if($drawing->technology == 'Sculpture') selected @endif value="Sculpture">Sculpture</option>
                                <option @if($drawing->technology == 'Mixed Technique') selected @endif value="Mixed Technique">Mixed Technique</option>
                                <option @if($drawing->technology == 'Engraving') selected @endif value="Engraving">Engraving</option>
                                <option @if($drawing->technology == 'Icon') selected @endif value="Icon">Icon</option>
                                <option @if($drawing->technology == 'Applied Art') selected @endif value="Applied Art">Applied Art</option>
                            </select>
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Status</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="status" class="form-control">
                                <option @if($drawing->status == 'For Sale') selected @endif value="For Sale">{{ $string['for_sale'] }}</option>
                                <option @if($drawing->status == 'Sold') selected @endif value="Sold">{{ $string['sold'] }}</option>
                                <option @if($drawing->status == 'Not for Sale') selected @endif value="Not for Sale">{{ $string['not_for_sale'] }}</option>
                                <option @if($drawing->status == 'Private Collection') selected @endif value="Private Collection">{{ $string['private_collection'] }}</option>
                                <option @if($drawing->status == 'Donated') selected @endif value="Donated">{{ $string['donated'] }}</option>
                                <option @if($drawing->status == 'Charity sale') selected @endif value="Charity sale">{{ $string['charity sale'] }}</option>
                                <option @if($drawing->status == 'Artists collection') selected @endif value="Artists collection">{{ $string['artists collection'] }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="size" class="col-md-4 col-form-label text-md-right">{{ $string['size'] }}</label>

                        <div class="row ml-6">
                            <input id="sizeOne" type="number" value="{{ explode(' ', $drawing->size)[0] }}" class="form-control col-md-2 mr-1" name="sizeOne" style="margin-left:25px;">
                            x
                            <input id="sizeTwo" type="number" value="{{ explode(' ', $drawing->size)[2] }}" class="form-control col-md-2 ml-1" name="sizeTwo">
                            <select class="ml-1" name="unit">
                                <option @if(explode(' ', $drawing->size)[3] == 'Cm') selected @endif value="Cm">Cm</option>
                                <option @if(explode(' ', $drawing->size)[3] == 'Dm') selected @endif value="Dm">Dm</option>
                            </select>
                            @if ($errors->has('size'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('size') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Price</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input id="price" type="number" class="form-control" name="price" value="{{ $drawing->price }}">
                            <small class="form-text text-muted">Description</small>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Artist id</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input id="price" type="number" class="form-control" name="price" value="{{ $drawing->artistId }}">
                            <small class="form-text text-muted">Artist id. Name of artist: {{ $drawing->getArtist->name }}</small>
                            <p>To find artist id (sign #) please go to page <a href="{{ route('admin.artists') }}">Artists</a></p>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="file-input" class=" form-control-label">Picture</label></div>
                        <div class="col-12 col-md-9">
                            @if($drawing->picture == ' ')
                                <p>This drawing has no picture yet</p>
                            @else
                                <img src="{{ asset($drawing->picture) }}" alt="" width="400px">
                            @endif
                            <input id="image" type="file" class="form-control" name="image">
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>

                    @if(Auth::user()->getRole != null && Auth::user()->getRole->title == 'Master')
                        <br>
                        @if($drawing->activated)
                            <a type="button" href="{{ route('drawing.remove.form',['id'=>$drawing->id]) }}" class="btn btn-secondary">{{ $string['deactivate'] }}</a>
                        @else
                            <a type="button" href="{{ route('drawing.activate',['id'=>$drawing->id]) }}" class="btn btn-secondary">{{ $string['activate'] }}</a>
                        @endif
                        <br>
                        <a type="button" href="{{ route('drawing.delete',['id'=>$drawing->id]) }}" class="btn btn-secondary">{{ $string['delete'] }}</a>
                    @endif

                    <button type="reset" class="btn btn-danger btn-sm" onclick="location.href='{{ URL::previous() }}';">
                        <i class="fa fa-ban"></i> Reset
                    </button>

                </div>
            </form>
        </div>
    </div>
@endsection