@extends('admin.layout.app')

@section('breadcrumbs')
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Events</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Events grid</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="content mt-3">

        <div class="col-sm-12 col-lg-12">
            <div class="card">

                <table class="table">
                    <form action="{{ route('admin.events') }}" method="GET">
                        <thead>
                        <tr>
                            <th scope="col">Search</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col">Search</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row"><input type="text" name="search" placeholder="Type your term" @if(isset($term)) value="{{ $term }}" @endif></th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><input type="submit" value="Search"></td>
                        </tr>
                        </tbody>
                    </form>
                </table>

                <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="{{ route('admin.event.create.form') }}">Create event</a></td>
                            </tr>
                        </tbody>

                </table>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">City</th>
                        <th scope="col">Country</th>
                        <th scope="col">Date of start</th>
                        <th scope="col">Date of fin</th>
                        <th scope="col">Description</th>
                        <th scope="col">Activated</th>
                        <th scope="col">Type</th>
                        <th scope="col">Link</th>
                        <th scope="col">Edit page</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($events as $event)
                            <tr>
                                <th scope="row">{{ $event->id }}</th>
                                <td>{{ $event->title }}</td>
                                <td>{{ $event->city }}</td>
                                <td>{{ $event->country }}</td>
                                <td>{{ $event->date_start }}</td>
                                <td>{{ $event->date_fin }}</td>
                                <td>{{ $event->description }}</td>
                                <td>{{ $event->activated }}</td>
                                <td>{{ $event->type }}</td>
                                <td>{{ $event->link }}</td>
                                <td><a href="{{ route('admin.event.update.form',['id'=>$event->id]) }}">Edit event {{ $event->name }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $events->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
        <!--/.col-->
    </div> <!-- .content -->
@endsection