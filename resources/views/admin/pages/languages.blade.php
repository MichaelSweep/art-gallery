@extends('admin.layout.app')

@section('breadcrumbs')
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Languages</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Languages grid</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="content mt-3">

        <div class="col-sm-12 col-lg-12">
            <div class="card">
                <table class="table">
                    <form action="{{ route('admin.language.create') }}" method="POST">
                        {{ csrf_field() }}
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Create new</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Create new language: </th>
                                <td><input type="text" name="title" placeholder="Title"></td>
                                <td><input type="submit" value="Create"></td>
                            </tr>
                        </tbody>
                    </form>
                </table>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Edit page</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($languages as $language)
                            <tr>
                                <th scope="row">{{ $language->id }}</th>
                                <td>{{ $language->title }}</td>
                                <td><a href="{{ route('admin.language.update.form',['id'=>$language->id]) }}">Edit role {{ $language->title }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $languages->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
        <!--/.col-->

    </div> <!-- .content -->
@endsection