@extends('admin.layout.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <form action="{{ route('admin.artist.update') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" name="artist_id" value="{{ $artist->id }}">
                <div class="card-header">
                    <strong>{{ $artist->name }}</strong> Updating Form
                </div>
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Artist name</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="name" placeholder="Artist name" value="{{ $artist->name }}" class="form-control"><small class="form-text text-muted">Artist name</small></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="email-input" class=" form-control-label">Email</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="email-input" name="email" placeholder="Enter Email" value="{{ $artist->email }}" class="form-control"><small class="form-text text-muted">User email</small></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="email-input" class=" form-control-label">Country</label></div>
                        <div class="col-12 col-md-9"><input type="text" id="email-input" name="country" placeholder="Enter Country" value="{{ $artist->country }}" class="form-control"><small class="form-text text-muted">User country</small></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Bio</label></div>
                        <div class="col-12 col-md-9"><textarea name="bio" id="textarea-input" rows="9" placeholder="Bio..." class="form-control">{{ $artist->bio }}</textarea></div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Position</label></div>
                        <div class="col-12 col-md-9"><textarea name="position" id="textarea-input" rows="9" placeholder="Position" class="form-control">{{ $artist->position }}</textarea></div>
                    </div>

                    @if(Auth::user()->getRole != null && Auth::user()->getRole->title == 'Master')
                        <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">Is admin</label></div>
                            <div class="col col-md-9">
                                <div class="form-check">
                                    <div class="radio">
                                        <label for="radio1" class="form-check-label ">
                                            <input type="radio" id="radio1" name="admin" value="1" @if($artist->isAdmin()) checked @endif class="form-check-input">Is admin
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label for="radio2" class="form-check-label ">
                                            <input type="radio" id="radio2" name="admin" value="0" @if(!$artist->isAdmin()) checked @endif class="form-check-input">Is not admin
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if(Auth::id() != $artist->id || (Auth::user()->getRole != null && Auth::user()->getRole->title == 'Master'))
                        <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">Choose role</label></div>
                            <div class="col col-md-9">
                                <div class="form-check">
                                    <div class="radio">
                                        <label for="radio-role-0" class="form-check-label ">
                                            <input type="radio" id="radio-role-0" name="role_id" value="0" @if($artist->getRole == null) checked @endif class="form-check-input">Artist
                                        </label>
                                    </div>
                                    @foreach($roles as $role)
                                        <div class="radio">
                                            <label for="radio-role-{{ $role->id }}" class="form-check-label ">
                                                <input type="radio" id="radio-role-{{ $role->id }}" name="role_id" value="{{ $role->id }}" @if($artist->getRole != null && $artist->getRole->id == $role->id) checked @endif class="form-check-input">{{ $role->title }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="row form-group">
                        <div class="col col-md-3"><label for="file-input" class=" form-control-label">Avatar</label></div>
                        <div class="col-12 col-md-9">
                            @if($artist->avatar == ' ')
                                <p>This user did not upload avatar yet</p>
                            @else
                                <img src="{{ asset($artist->avatar) }}" alt="" width="400px">
                            @endif
                            <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image">
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>

                    <button type="reset" class="btn btn-danger btn-sm" onclick="location.href='{{ URL::previous() }}';">
                        <i class="fa fa-ban"></i> Reset
                    </button>

                    @if(!$artist->isAdmin() || ($artist->getRole != null && $artist->getRole->title != 'Master'))
                        @if(Auth::user()->getRole != null && Auth::user()->getRole->title == 'Master')
                            <br>
                            @if($artist->activated)
                                <a type="button" href="{{ route('artist.remove',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['deactivate'] }}</a>
                            @else
                                <a type="button" href="{{ route('artist.activate',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['activate'] }}</a>
                            @endif
                            <br>
                            <a type="button" href="{{ route('artist.delete',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['delete'] }}</a>
                        @endif
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection