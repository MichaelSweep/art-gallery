@extends('admin.layout.app')

@section('breadcrumbs')
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Artists</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Artists grid</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="content mt-3">

        <div class="col-sm-12 col-lg-12">
            <div class="card">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Country</th>
                        <th scope="col">Email</th>
                        <th scope="col">bio</th>
                        <th scope="col">activated</th>
                        <th scope="col">Action</th>
                        <th scope="col">is admin</th>
                        <th scope="col">Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($artists as $artist)
                            <tr>
                                <th scope="row">{{ $artist->id }}</th>
                                <td>{{ $artist->name }}</td>
                                <td>{{ $artist->country }}</td>
                                <td>{{ $artist->email }}</td>
                                <td>{{ limit_text($artist->bio,14) }}</td>
                                <td>
                                    {{ $artist->activated }}
                                </td>
                                <td>
                                    @if(!$artist->isAdmin() || ($artist->getRole != null && $artist->getRole->title != 'Master'))
                                        @if(Auth::user()->getRole->title == 'Master')
                                            @if($artist->activated)
                                                <a type="button" href="{{ route('artist.remove',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['deactivate'] }}</a>
                                            @else
                                                <a type="button" href="{{ route('artist.activate',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['activate'] }}</a>
                                            @endif
                                            <br>
                                            <a type="button" href="{{ route('artist.delete',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['delete'] }}</a>
                                        @endif
                                    @endif
                                </td>
                                <td>{{ $artist->admin }}</td>
                                <td><a href="{{ route('admin.artist.update.form',['id'=>$artist->id]) }}">Edit user {{ $artist->name }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $artists->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
        <!--/.col-->

    </div> <!-- .content -->
@endsection