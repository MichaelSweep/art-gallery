@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $drawing->title }} by {{ $drawing->getArtist->name }}
    </title>
@endsection

@section('content')

    <div class="container">
        <div class="row content-row" style="position: relative">
            <div class="prev-link" style="position: absolute; top:40%;left:-70px;">
                <a href="{{ route('drawing.single',['id'=>$prev_image_id]) }}">
                    <img src="{{ asset('images/prev.png') }}" alt="">
                </a>
            </div>
            <div class="card col-md-7 full-drawing">
                <a class="example-image-link" href="{{ asset($drawing->picture) }}" data-lightbox="example-set" data-title="{{ $drawing->description }} <br><a class='btn btn-outline-success' style='background: #28a745; color:white;margin-top:10px;'
                href='{{ route('site.email.send.drawing',['id'=>$drawing->id]) }}'>{{ $string['im_interested'] }}</a>">
                    <img class="card-img-top img-fluid img-rounded example-image" src="{{ asset($drawing->picture) }}">
                </a>
            </div>
            <div class="card col-md-4 ml-md-2">
                <h1 class="card-title text-lg-center mt-md-2" style="font-size: 25px;">{{$drawing->title}}</h1>
                <ul class="list-group list-group-flush">
                    <a href="{{ route('artist.single',['id'=>$drawing->artistId]) }}">
                        <li class="list-group-item">{{ $string['author'] }}
                            : {{\App\User::find($drawing->artistId)->name}}</li>
                    </a>
                    <li class="list-group-item">{{ $string['country'] }}: {{$drawing->country}}</li>
                    <li class="list-group-item">{{ $string['genre'] }}: {{$drawing->genre}}</li>
                    <li class="list-group-item">{{ $string['media'] }}: {{$drawing->technology}}</li>
                    <li class="list-group-item">{{ $string['size'] }}: {{$drawing->size}}</li>
                    <li class="list-group-item">{{ $string['status'] }}: {{$drawing->status}}</li>
                    <li class="list-group-item" style="max-height:300px; overflow: auto">{{$drawing->description}}</li>
                </ul>
                <div class="card-footer mt-md-5">
                    <h3 class="card-title">{{ $string['price'] }}: {{$drawing->price}}€</h3>
                    <small class="text-muted">{{$drawing->date}}</small>
                </div>
                <a class="btn btn-outline-success" style="background: #28a745; color:white;"
                   href="{{ route('site.email.send.drawing',['id'=>$drawing->id]) }}">{{ $string['im_interested'] }}</a>
                @if(Auth::check())
                    @if(Auth::user()->id==$drawing->artistId|| Auth::user()->admin)
                        <a type="button" href="{{ route('drawing.update.form',['id'=>$drawing->id])}}"
                           class="btn btn-secondary">{{ $string['edit'] }}</a>
                        <a type="button" href="{{ route('drawing.remove.form',['id'=>$drawing->id]) }}"
                           class="btn btn-secondary">{{ $string['deactivate'] }}</a>
                        <a type="button" href="{{ route('drawing.delete',['id'=>$drawing->id]) }}"
                           class="btn btn-secondary">{{ $string['delete'] }}</a>
                    @endif
                @endif
            </div>
            <div class="prev-link" style="position: absolute; top:40%;right:0px;">
                <a href="{{ route('drawing.single',['id'=>$next_image_id]) }}">
                    <img src="{{ asset('images/next.png') }}" alt="">
                </a>
            </div>
        </div>
    </div>

@endsection

@section('css_includes')
    <link href="{{ asset('css/lightbox.css') }}" rel="stylesheet">
@endsection

@section('js_includes')
    <script src="{{ asset('js/lightbox.js') }}"></script>


    <script>
        var next_href = "{{ route('drawing.single',['id' =>$next_image_id]) }}";
        var prev_href = "{{ route('drawing.single',['id' =>$prev_image_id]) }}";
        $(".full-drawing").simpleGal();
    </script>
@endsection