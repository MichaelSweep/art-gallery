@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $string['update'] }}</div>

                    <div class="card-body">
                        <form action="{{route('drawing.update')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input id="id" name="id" type="hidden" value="{{$drawing->id}}">
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">{{ $string['title'] }}</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $drawing->title}}" required autofocus maxlength="120">

                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">{{ $string['city'] }}</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ $drawing->city }}">

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="country" class="col-md-4 col-form-label text-md-right">{{ $string['country'] }}</label>

                                <div class="col-md-6">
                                    <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ $drawing->country }}">

                                    @if ($errors->has('country'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">{{ $string['date'] }}</label>

                                <div class="col-md-6">
                                    <input id="date" type="date" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" value="{{ $drawing->date }}">

                                    @if ($errors->has('date'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">{{ $string['description'] }}</label>

                                <div class="col-md-6">
                                    <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" maxlength="1000">{{ $drawing->description }}</textarea>

                                    @if($errors->has('description'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="genre" class="col-md-4 col-form-label text-md-right">{{ $string['genre'] }}</label>

                                <div class="col-md-6">
                                    <select name="genre" class="form-control">
                                        <option @if($drawing->genre == 'Landscape') selected @endif value="Landscape">Landscape</option>
                                        <option @if($drawing->genre == 'Portrait') selected @endif value="Portrait">Portrait</option>
                                        <option @if($drawing->genre == 'Still life') selected @endif value="Still life">Still life</option>
                                        <option @if($drawing->genre == 'Religious') selected @endif value="Religious">Religious</option>
                                        <option @if($drawing->genre == 'Nude') selected @endif value="Nude">Nude</option>
                                        <option @if($drawing->genre == 'Abstract') selected @endif value="Abstract">Abstract</option>
                                        <option @if($drawing->genre == 'Poster') selected @endif value="Poster">Poster</option>
                                    </select>
                                    @if ($errors->has('genre'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('genre') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="technology" class="col-md-4 col-form-label text-md-right">{{ $string['technology'] }}</label>

                                <div class="col-md-6">

                                    <select name="technology" class="form-control">
                                        <option @if($drawing->technology == 'Oil on canvas') selected @endif value="Oil on canvas">Oil on canvas</option>
                                        <option @if($drawing->technology == 'Oil on paper') selected @endif value="Oil on paper">Oil on paper</option>
                                        <option @if($drawing->technology == 'Pastel') selected @endif value="Pastel">Pastel</option>
                                        <option @if($drawing->technology == 'Acrylic') selected @endif value="Acrylic">Acrylic</option>
                                        <option @if($drawing->technology == 'Pencil') selected @endif value="Pencil">Pencil</option>
                                        <option @if($drawing->technology == 'Watercolor') selected @endif value="Watercolor">Watercolor</option>
                                        <option @if($drawing->technology == 'Ink') selected @endif value="Ink">Ink</option>
                                        <option @if($drawing->technology == 'Batik') selected @endif value="Batik">Batik</option>
                                        <option @if($drawing->technology == 'Fresco') selected @endif value="Fresco">Fresco</option>
                                        <option @if($drawing->technology == 'Gouache') selected @endif value="Gouache">Gouache</option>
                                        <option @if($drawing->technology == 'Enamel') selected @endif value="Enamel">Enamel</option>
                                        <option @if($drawing->technology == 'Spray paint') selected @endif value="Spray paint">Spray paint</option>
                                        <option @if($drawing->technology == 'Tempera') selected @endif value="Tempera">Tempera</option>
                                        <option @if($drawing->technology == 'Digital painting') selected @endif value="Digital painting">Digital painting</option>
                                        <option @if($drawing->technology == 'Photo') selected @endif value="Photo">Photo</option>
                                        <option @if($drawing->technology == 'Sculpture') selected @endif value="Sculpture">Sculpture</option>
                                        <option @if($drawing->technology == 'Mixed Technique') selected @endif value="Mixed Technique">Mixed Technique</option>
                                        <option @if($drawing->technology == 'Engraving') selected @endif value="Engraving">Engraving</option>
                                        <option @if($drawing->technology == 'Icon') selected @endif value="Icon">Icon</option>
                                        <option @if($drawing->technology == 'Applied Art') selected @endif value="Applied Art">Applied Art</option>
                                    </select>

                                    @if ($errors->has('technology'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('technology') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="size" class="col-md-4 col-form-label text-md-right">{{ $string['size'] }}</label>

                                <div class="row ml-3">
                                    <input id="sizeOne" type="number" class="form-control col-md-2 mr-1{{ $errors->has('sizeOne') ? ' is-invalid' : '' }}" value="{{ explode(' ', $drawing->size)[0] }}" name="sizeOne">
                                    x
                                    <input id="sizeTwo" type="number" class="form-control col-md-2 ml-1{{ $errors->has('sizeTwo') ? ' is-invalid' : '' }}" value="{{ explode(' ', $drawing->size)[2] }}" name="sizeTwo">
                                    <select class="ml-1" name="unit">
                                        <option value="Cm">Cm</option>
                                        <option value="Dm">Dm</option>
                                    </select>
                                    @if ($errors->has('size'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('size') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="status" class="col-md-4 col-form-label text-md-right">{{ $string['status'] }}</label>

                                <div class="col-md-6">
                                    <select name="status" class="form-control">
                                        <option value="For Sale">For Sale</option>
                                        <option value="Sold">Sold</option>
                                        <option value="Not for Sale">Not for Sale</option>
                                        <option value="Private Collection">Private Collection</option>
                                        <option value="Donated">Donated</option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-4 col-form-label text-md-right">{{ $string['price'] }}</label>

                                <div class="col-md-3">
                                    <input id="price" type="number" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" value="{{$drawing->price}}">

                                    @if ($errors->has('price'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col" style="position: relative;">€</div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ $string['update'] }}
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection