@extends('layouts.app')

@section('content')

    <div class="container">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel mt-md-5">
            <div class="container">
                <form class="form-inline pull-xs-right" action="{{route('artist.find')}}" method="post">
                    {{ csrf_field() }}
                    <select id="column" name="column" class="form-group form-control mr-md-2">
                        <option value="name">{{ $string['name'] }}</option>
                        <option value="city">{{ $string['city'] }}</option>
                        <option value="country">{{ $string['country'] }}</option>
                    </select>

                    <div class="form-group">
                        <input id="search" type="text" class="form-control{{ $errors->has('search') ? ' is-invalid' : '' }}" name="search" placeholder="{{ $string['search']}}" required autofocus>

                        @if ($errors->has('search'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('search') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-outline-success ml-md-2">
                        {{ $string['search'] }}
                    </button>
                </form>
            </div>
        </nav>
        <div class="card-columns py-5 bg-light ml-4">

            @foreach($artists as $artist)
                @if(!$artist->activated)
                    <a href="{{ route('artist.single',['id'=>$artist->id]) }}" style="text-decoration: none; color: black;">
                        <div class="card box-shadow ">
                            <img class="img-fluid rounded float-right mb-md-3" src="../{{$artist->avatar}}" alt="">
                            <div class="card-body">
                                <h5 class="card-title">{{$artist->name}}</h5>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">{{ $string['email'] }}: {{$artist->email}}</li>
                                    <li class="list-group-item">{{ $string['country'] }}: {{$artist->country}}</li>
                                    <li class="list-group-item">BIO: {{$artist->bio}}</li>
                                </ul>
                            </div>
                            @if(Auth::check())
                                @if(Auth::user()->admin)
                                    <a type="button" href="{{ route('artist.activate',['id'=>$artist->id]) }}"class="btn btn-secondary">{{ $string['activate'] }}</a>
                                @endif
                            @endif
                        </div>


                    </a>
                @endif
            @endforeach
        </div>
    </div>
@endsection