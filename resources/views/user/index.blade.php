@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $string['artists'] }}
    </title>
@endsection

@section('content')
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel mt-md-5">
            <div class="container">
                <ul class="nav navbar-nav row">
                    <li class="nav-item">
                        <button class="btn btn-link nav-link ml-md-3">@sortablelink('name',__('messages.sortbyname'))</button>
                    </li>
                    <li class="nav-item">
                        <button class="btn btn-link nav-link ml-md-3">@sortablelink('created_at',__('messages.sortbynewness'))</button>
                    </li>
                </ul>
                <form class="form-inline pull-xs-right" action="{{route('artists')}}" method="GET">
                    <div class="form-group">
                        <input id="search" type="text" class="form-control{{ $errors->has('search') ? ' is-invalid' : '' }}"
                               name="search" placeholder="{{ $string['search_by_name'] }}" autofocus
                                @if(isset($term)) value="{{ $term }}" @endif>

                        @if($errors->has('search'))
                            <span class="invalid-feedback">
                            <strong>{{ $errors->first('search') }}</strong>
                        </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-outline-success ml-md-2">
                        {{ $string['search'] }}
                    </button>
                </form>
            </div>
        </nav>
        <div class="row content-row masonry-grid">
            @foreach($artists as $artist)
                @if((Auth::check() && Auth::user()->isAdmin()) || $artist->activated)
                    @if($artist->getRole != null && $artist->getRole->show_in_artists || $artist->getRole == null)
                        <div class="col-md-4 masonry-item">
                            <a href="{{ route('artist.single',['id'=>$artist->id]) }}" style="text-decoration: none; color: black;">
                                <div class="card box-shadow ">
                                    <img class="img-fluid rounded float-right mb-md-3" src="{{ $artist->avatar }}" alt="">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$artist->name}}</h5>
                                        <ul class="list-group list-group-flush">
                                            @if($artist->position != null)
                                                <li class="list-group-item">{{$artist->position}}</li>
                                            @endif
                                            <li class="list-group-item">{{ $string['country'] }}: {{$artist->country}}</li>
                                            <li class="list-group-item">BIO: {{ limit_text($artist->bio, 55)}}</li>
                                        </ul>
                                    </div>
                                    @if(Auth::check())
                                        @if(
                                                Auth::user()->admin
                                                && Auth::user()->getRole != null
                                                && Auth::user()->getRole->title == 'Master'
                                                &&
                                                (
                                                    $artist->getRole == null
                                                    ||
                                                    ($artist->getRole != null && $artist->getRole->title != 'Master')
                                                )
                                            )

                                            @if(!$artist->activated)
                                                <div class="card-footer alert-warning">User is deactivated</div>
                                                <a type="button" href="{{ route('artist.activate',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['activate'] }}</a>
                                            @else
                                                <a type="button" href="{{ route('artist.remove',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['deactivate'] }}</a>
                                            @endif
                                            @if($artist->id != Auth::id())
                                                <a type="button" href="{{ route('artist.delete',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['delete'] }}</a>
                                            @endif
                                                <a type="button" href="{{ route('artist.update.form',['id'=>$artist->id]) }}" class="btn btn-secondary">{{ $string['edit'] }}</a>
                                        @endif
                                    @endif
                                </div>
                            </a>
                        </div>
                    @endif
                @endif
            @endforeach
        </div>
        {!! $artists->appends(\Request::except('page'))->render() !!}
    </div>
@endsection

@section('js_includes')
    <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <script>
        var $container = $('.masonry-grid');
        $container.imagesLoaded(function(){
            $container.masonry({
                itemSelector : '.masonry-item',
                columnWidth : 0
            });
        });
    </script>
@endsection