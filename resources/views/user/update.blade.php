@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - update {{ $artist->name }}
    </title>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $string['update'] }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('artist.update') }}">
                            {{ csrf_field() }}
                            <input type="hidden" id="id" name="id" value="{{$artist->id}}">
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">{{ $string['name'] }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ $artist->name }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="country"
                                       class="col-md-4 col-form-label text-md-right">{{ $string['country'] }}</label>

                                <div class="col-md-6">
                                    <input id="country" type="text"
                                           class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}"
                                           name="country" value="{{ $artist->country}}">

                                    @if ($errors->has('country'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            @if(Auth::user()->isAdmin())
                                <div class="form-group row">
                                    <label for="country"
                                           class="col-md-4 col-form-label text-md-right">{{ $string['here_since'] }}</label>

                                    <div class="col-md-6">
                                        {{ $artist->created_at->format('d.m.Y') }}
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="bio" class="col-md-4 col-form-label text-md-right">{{ $string['bio'] }}</label>

                                <div class="col-md-6">
                                    <textarea id="bio" type="text" rows='3'
                                              class="form-control{{ $errors->has('bio') ? ' is-invalid' : '' }}"
                                              name="bio" value="{{ $artist->bio}}">{{ $artist->bio}}</textarea>

                                    @if ($errors->has('bio'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('bio') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="position" class="col-md-4 col-form-label text-md-right">{{ $string['position'] }}</label>

                                <div class="col-md-6">
                                    <input id="position" type="text" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="position" value="{{ $artist->position }}">

                                    @if ($errors->has('position'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('position') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ $string['update'] }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
