@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $string['management'] }}
    </title>
@endsection

@section('content')

    <div class="container">
        <div class="card-columns py-5 bg-light ml-4 row content-row">
            @foreach($artists as $artist)
                @if($artist->getRole != null && $artist->getRole->show_in_management && $artist->activated)
                    <div class="col-md-4">
                        <a href="{{ route('artist.single',['id'=>$artist->id]) }}" style="text-decoration: none; color: black;">
                            <div class="card box-shadow ">
                                <img class="img-fluid rounded float-right mb-md-3" src="{{asset($artist->avatar)}}" alt="">
                                <div class="card-body">
                                    <h5 class="card-title">{{$artist->name}}</h5>
                                    <ul class="list-group list-group-flush">
                                        @if($artist->position != null)
                                            <li class="list-group-item">{{$artist->position}}</li>
                                        @endif
                                        <li class="list-group-item">{{ $string['country'] }}: {{$artist->country}}</li>
                                        <li class="list-group-item">BIO: {{ limit_text($artist->bio, 55)}}</li>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection