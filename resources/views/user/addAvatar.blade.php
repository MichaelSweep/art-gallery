@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $string['add_avatar'] }}
    </title>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $string['add_avatar'] }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('artist.add.avatar') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input name="id" id="id" value="{{$artist->id}}" hidden>
                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">{{ $string['avatar'] }}</label>
                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" required>
                                    <small class="form-text text-muted">
                                        Accepted file types: jpg,jpeg,png,gif,bmp<br>
                                        Maximum accepted file size: 6Mb<br>
                                        Recommended picture size: 1Mb-5Mb
                                    </small>

                                    @if ($errors->has('image'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ $string['add'] }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
