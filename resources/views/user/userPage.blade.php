@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $string['artists collection'] }}
    </title>
@endsection

@section('content')
@if($artist != null)
    <div class="container user-page">
        <div class="jumbotron jumbotron-fluid row">
            <div class="col-md-7">
                <h2 class="display-3">{{$artist->name}}</h2>
                <p class="lead">{{ $string['country'] }}: {{$artist->country}}</p>
                <p class="lead">{{ $string['here_since'] }}: {{$artist->created_at->format('Y')}}</p>
                @if($artist->position != '') <p class="lead">{{$artist->position}}</p> @endif
                <p class="lead">{{ $string['bio'] }}: {{$artist->bio}}</p>
                @if($artist->role!='artist')
                    <p class="lead">{{ $string['role'] }}: {{$artist->role}}</p>
                @endif
                @if(Auth::check())
                    @if(Auth::user()->id==$artist->id || Auth::user()->admin)
                        <div class="mt-md-5">
                            <a type="button"
                               href="{{ route('artist.update.form',['id'=>$artist->id]) }}"
                               class="btn btn-secondary">{{ $string['edit'] }}</a>
                            @if($artist->id != Auth::id())
                                @if($artist->activated)
                                    <a type="button" href="{{ route('artist.remove',['id'=>$artist->id]) }}"
                                       class="btn btn-secondary">{{ $string['deactivate'] }}</a>
                                @else
                                    <a type="button" href="{{ route('artist.activate',['id'=>$artist->id]) }}"
                                       class="btn btn-secondary">{{ $string['activate'] }}</a>
                                @endif
                                <a type="button" href="{{ route('artist.delete',['id'=>$artist->id]) }}"
                                   class="btn btn-secondary">{{ $string['delete'] }}</a>
                            @endif
                            <a type="button"
                               href="{{ route('artist.add.avatar.form',['id'=>$artist->id]) }}"
                               class="btn btn-secondary">{{ $string['add_avatar'] }}</a>
                            @if(Auth::user()->admin && (Auth::user()->getRole != null && Auth::user()->getRole->title == 'Master' ))
                                <a type="button"
                                   href="{{ route('artist.set.role.form',['id'=>$artist->id]) }}"
                                   class="btn btn-secondary">{{ $string['set_role'] }}</a>
                            @endif
                        </div>
                    @endif
                @endif
            </div>
            <div class="col-md-4 float-right">
                <img class="img-fluid rounded float-right" src="{{ asset($artist->avatar) }}" alt="">
            </div>
        </div>
        <div class="card-columns row content-row masonry-grid">
            @foreach(\App\Drawing::where('artistId', $artist->id)->get() as $drawing)
                @if($drawing->activated || (Auth::check() && Auth::user()->admin) || (Auth::check()) && Auth::id() == $drawing->getArtist->id)
                    <div class="col-md-4 masonry-item">
                        <a href="{{ route('drawing.single',['id'=>$drawing->id]) }}"
                           style="text-decoration: none; color: black;">
                            <div class="card box-shadow">
                                <img class="card-img-top img-fluid" src="{{ asset($drawing->picture) }}">
                                <div class="card-body">
                                    <h5 class="card-title text-lg-center">{{$drawing->title}}</h5>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">{{ $string['country'] }}: {{$drawing->country}}</li>
                                        <li class="list-group-item">{{ $string['genre'] }}: {{$drawing->genre}}</li>
                                        <li class="list-group-item">{{ $string['technology'] }}
                                            : {{$drawing->technology}}</li>
                                        <li class="list-group-item">{{ $string['size'] }}: {{$drawing->size}}</li>
                                    </ul>
                                    <div class="card-footer">
                                        <h3 class="card-title">{{ $string['price'] }}: {{$drawing->price}}€</h3>
                                        <small class="text-muted">{{$drawing->date}}</small>
                                        @if(Auth::check() && Auth::id() == $drawing->getArtist->id && !$drawing->activated)
                                            <h3 class="card-title" style="color:red;">NOT ACTIVE! People can not see this piece of art in Gallery</h3>
                                        @endif
                                    </div>
                                    <div class="btn-group" role="group">
                                        @if(Auth::check())
                                            @if(Auth::user()->id==$artist->id || Auth::user()->admin)
                                                <a type="button"
                                                   href="{{ route('drawing.update.form',['id'=>$drawing->id]) }}"
                                                   class="btn btn-secondary">{{ $string['edit'] }}</a>

                                                @if($drawing->activated)
                                                    <a type="button"
                                                       href="{{ route('drawing.remove.form',['id'=>$drawing->id]) }}"
                                                       class="btn btn-secondary">{{ $string['deactivate'] }}</a>
                                                @else
                                                    <a type="button"
                                                       href="{{ route('drawing.activate',['id'=>$drawing->id]) }}"
                                                       class="btn btn-secondary">{{ $string['activate'] }}</a>
                                                @endif
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endif
@endsection

@section('js_includes')
    <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            var $container = $('.masonry-grid');
            $container.imagesLoaded(function () {
                $container.masonry({
                    itemSelector: '.masonry-item',
                    columnWidth: 0
                });
            });
        });
    </script>
@endsection