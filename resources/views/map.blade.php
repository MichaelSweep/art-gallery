@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $string['map'] }}
    </title>
@endsection

@section('content')
    <div class="container">
    <div class="jumbotron row">
        <div class="col-md-6">
            <p class="lead">Come to visit us any time at Miho Gallery in Limassol, Cyprus</p>
            <p class="lead">AMA Cyprus address is:</p>
            <p class="lead">Chrysanthou Mylona 5, P. Lordos Bld., Block B Limassol, Cyprus 3030</p>

            <p class="lead">AMA Cyprus telephone numbers:</p>
            <p class="lead">+357 25 341463</p>
            <p class="lead">+357 25 760 050</p>
            <p class="lead">+357 25 342 535</p>
            <p class="lead">Or you can even send us a fax if you still remember how to do this</p>
            <p class="lead">Fax: +357 25 760 005</p>
            <p class="lead">Send us your wise suggestions and positive comments using mail form in Membership section of the website.</p>
            <p class="lead">Follow us on Facebook: <a href="http://www.facebook.com/amacyprus/">www.facebook.com/amacyprus/</a></p>
            <p class="lead">and even on Instagram: <a href="http://www.instagram.com/amacyprus/">www.instagram.com/amacyprus/</a></p>
        </div>
        <div id='map' class="card col-md-5 float-right">
        </div>
    </div>
    </div>
@endsection

@section('js_includes')
    <script src="{{ asset('js/mapjs.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEp6PZ-wytWuFa9uPVqXgdzmQVCBHku34&callback=initMap"
            async defer>
    </script>
@endsection
