<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117665209-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-117665209-1');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter48395951 = new Ya.Metrika({
                        id:48395951,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/48395951" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta_tags', '<title>Amacyprus</title>')
    <meta name="description"
          content="Association of Mediterranean Artists is Cyprus-based non-profit organization.
          We organize art exhibitions in Cyprus, provide training and support for artists in Cyprus.
          You can buy and sell paintings and works of art our online art gallery.
          Join AMA Cyprus to participate in charity events, seminars, workshops, master classes!" />
    @if(Auth::check() && Auth::user()->language_id == 2)
        <meta name="keywords" content="Association of Mediterranean Artists, AMA Cyprus, buy paintings in Cyprus, sell paintings in Cyprus, art exhibitions in Cyprus,
              Cyprus Artists, Fine art in Cyprus, Cyprus Art Gallery" />
    @elseif(Auth::check() && Auth::user()->language_id == 1)
        <meta name="keywords" content="Ассоциация художников Средиземноморья, АХС Кипр, купить картины на Кипре, продать картины на Кипре, художественные выставки на Кипре,
               Кипрские художники, изобразительное искусство на Кипре, галерея искусств Кипра" />
    @endif

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hover.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('css_includes')
    <link rel="shortcut icon" href="/images/icon.png" type="image/png">
    <meta name="norton-safeweb-site-verification"
          content="69y3rsydc1ksgv-o6gzxx8i6yn-mzxhowrtmnrbngqlbaw88bxw2wqd5f3wzn7ggm7a0nqa1xott4as87758c0s-yq5nl40gbssylqgz0x9v863rn-69b2d6im5eqcem"/>
</head>
<body>
<div id="app">
    <div class="fixed-top">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a href="{{ route('site.info') }}"><img src="/images/logo.jpg"></a>
                    </li>
                </ul>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse flex-column navbar-collapse" id="navbarSupportedContent">
                    <h1 class="text-info">{{ $string['A_O_M_A'] }}</h1>
                    <div class="navbar-nav flex-row">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto col-sm-12">
                            <li class="nav-item">
                                <a class="nav-link btn btn-light mr-1 @if(is_current_page('drawings')) active @endif" role="button" href="{{ url('/') }}">
                                    {{ $string['art_gallery'] }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-light mr-1 @if(is_current_page('site.info')) active @endif" role="button"
                                   href="{{ route('site.info') }}">{{ $string['about_ama'] }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-light mr-1 @if(is_current_page('artists')) active @endif" role="button"
                                   href="{{ route('artists') }}">{{ $string['artists'] }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-light mr-1 @if(is_current_page('events')) active @endif" role="button"
                                   href="{{ route('events') }}">{{ $string['events'] }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-light mr-1 @if(is_current_page('site.management')) active @endif" role="button"
                                   href="{{ route('site.management') }}">{{ $string['management'] }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link btn btn-light mr-1 @if(is_current_page('site.email')) active @endif" role="button"
                                   href="{{ route('site.email') }}">{{ $string['membership'] }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-light mr-1 @if(is_current_page('site.map')) active @endif" role="button"
                                   href="{{ route('site.map') }}">{{ $string['be_our_guest'] }}</a>
                            </li>
                            <!-- Authentication Links -->
                            @guest
                            <li class="nav-item"><a class="nav-link btn btn-light mr-1 @if(is_current_page('login')) active @endif" href="{{ route('login') }}">{{ $string['login'] }}</a></li>
                            <li class="nav-item"><a class="nav-link btn btn-light mr-1 @if(is_current_page('register')) active @endif" href="{{ route('register') }}">{{ $string['register'] }}</a></li>
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('artist.single',['id'=>Auth::user()->id]) }}">
                                            {{ $string['myprofile'] }}
                                        </a>

                                        @if(Auth::user()->activated)
                                            <a class="dropdown-item" role="button"
                                               href="{{ route('drawing.create.form') }}">{{ $string['upload drawing'] }}</a>
                                        @endif

                                        <a role="button" class="dropdown-item" href="{{ route('site.change.locale',['id'=>1]) }}">Русский язык</a>
                                        <a role="button" class="dropdown-item" href="{{ route('site.change.locale',['id'=>2]) }}">English</a>

                                        @if(Auth::check())
                                            @if(Auth::user()->admin)

                                                <a class="dropdown-item" role="button"
                                                   href="{{ route('artist.activate.form') }}">{{ $string['activate'] }}</a>
                                                <a class="dropdown-item" role="button"
                                                   href="{{ route('admin.index') }}">Admin page</a>

                                            @endif
                                        @endif

                                        <a href="{{ route('artist.add.avatar.form',['id'=>Auth::id()]) }}"
                                           class="dropdown-item">{{ $string['add_avatar'] }}</a>

                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ $string['logout'] }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <main>
        @yield('content')
    </main>
</div>
<!-- Scripts -->
@yield('js_includes')
</body>
</html>
