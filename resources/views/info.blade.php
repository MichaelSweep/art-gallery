@extends('layouts.app')

@section('meta_tags')
<title>
    Amacyprus - {{ $string['info_title'] }}
</title>
@endsection

@section('content')
    <div class="container">
        <div class="jumbotron">
            {!! $string['info'] !!}
        </div>
    </div>
@endsection