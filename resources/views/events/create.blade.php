@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $string['add_event'] }}
    </title>
@endsection


@section('content')
    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $string['create_event'] }}</div>

                    <div class="card-body">
                        <form action="{{route('event.create')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ $string['title'] }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="name" value="{{ old('title') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="place" class="col-md-4 col-form-label text-md-right">{{ $string['place'] }}</label>

                                <div class="col-md-6">
                                    <input id="place" type="text" class="form-control{{ $errors->has('place') ? ' is-invalid' : '' }}" name="place" value="{{ old('place') }}">

                                    @if ($errors->has('place'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('place') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">{{ $string['city'] }}</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}">

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="country" class="col-md-4 col-form-label text-md-right">{{ $string['country'] }}</label>

                                <div class="col-md-6">
                                    <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}">

                                    @if ($errors->has('country'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date_start" class="col-md-4 col-form-label text-md-right">{{ $string['date_start'] }}</label>

                                <div class="col-md-6">
                                    <input id="date_start" type="date" class="form-control{{ $errors->has('date_start') ? ' is-invalid' : '' }}" name="date_start" value="{{ old('date_start') }}">

                                    @if ($errors->has('date_start'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('date_start') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date_fin" class="col-md-4 col-form-label text-md-right">{{ $string['date_fin'] }}</label>

                                <div class="col-md-6">
                                    <input id="date_fin" type="date" class="form-control{{ $errors->has('date_fin') ? ' is-invalid' : '' }}" name="date_fin" value="{{ old('date_fin') }}">

                                    @if ($errors->has('date_fin'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('date_fin') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">{{ $string['description'] }}</label>

                                <div class="col-md-6">
                                    <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" maxlength="300">{{ old('description') }}</textarea>

                                    @if($errors->has('description'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="type" class="col-md-4 col-form-label text-md-right">{{ $string['type'] }}</label>

                                <div class="col-md-6">
                                    <input id="type" type="text" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" value="{{ old('type') }}" maxlength="255">

                                    @if($errors->has('type'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('type') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="link" class="col-md-4 col-form-label text-md-right">{{ $string['link'] }}</label>

                                <div class="col-md-6">
                                    <input id="link" type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" name="link" value="{{ old('link') }}" maxlength="255">

                                    @if($errors->has('link'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('link') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">{{ $string['image'] }}</label>

                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image">

                                    @if ($errors->has('image'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ $string['submit'] }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection