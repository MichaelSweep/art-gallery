@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $string['events'] }}
    </title>
@endsection

@section('css_includes')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection

@section('content')
<div class="container">
    <div class="row content-row">
        <div class="col-md-2">
            <p>{{ $string['view as'] }}</p>
        </div>
        <div class="col-md-5">
            <a class="nav-link btn btn-light" role="button"
               href="{{ route('events.calendar') }}">{{ $string['calendar'] }}</a>
        </div>
        <div class="col-md-5">
            <a class="nav-link btn btn-light" role="button"
               href="{{ route('events') }}">{{ $string['chronologically'] }}</a>
        </div>
    </div>
    {!! $calendar->calendar() !!}
    {!! $calendar->script() !!}
</div>
@endsection

@section('js_includes')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
@endsection