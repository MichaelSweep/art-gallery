@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $event->title }}
    </title>
@endsection

@section('content')

    <div class="container">
        @if(Auth::check())
            @if(Auth::user()->admin)
                <div class="card-columns py-5 bg-light ml-4">
                    <a class="nav-link btn btn-light mr-1" role="button"
                       href="{{ route('event.create.form') }}">{{ $string['add_event'] }}</a>
                </div>
            @endif
        @endif
        <div class="row content-row">
            <div class="col-md-12 event-card">
                <div class="row">
                    <div class="col-md-12">
                        <p>{{ Carbon\Carbon::parse($event->date_start)->format('M d, Y') }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-1" style="text-align: right; margin-top: 100px;">
                        <a href="{{ route('event.single',['id'=>$prev_event_id]) }}"><img src="/images/prev.png" alt=""></a>
                    </div>
                    <div class="col-md-8 col-xs-10 event-card">
                        <p>
                            <img src="/{{ $event->image }}" alt="" style="width:100%;">
                        </p>
                        <a href="{{ route('event.single',['id'=>$event->id]) }}">
                            <strong>{{ $event->name }}</strong>
                        </a>
                        <p>
                            {{ Carbon\Carbon::parse($event->date_start)->format('d.m.Y') }} - {{ Carbon\Carbon::parse($event->date_fin)->format('d.m.Y') }}
                        </p>
                        <p>
                            {{ $event->place }}, {{ $event->city }}, {{ $event->country }}
                        </p>
                        <p>
                            {!! $event->description !!}
                        </p>

                        @if(Auth::check() && Auth::user()->admin)
                            <a class="btn btn-secondary" style="color:white;" href="{{ route('event.update.form',['id'=>$event->id]) }}">
                                {{ $string['edit'] }}
                            </a>
                            @if($event->activated)
                                <a class="btn btn-secondary" style="color:white;" href="{{ route('event.deactivate',['id'=>$event->id]) }}">
                                    {{ $string['deactivate'] }}
                                </a>
                            @else
                                <a class="btn btn-secondary" style="color:white;" href="{{ route('event.activate',['id'=>$event->id]) }}">
                                    {{ $string['activate'] }}
                                </a>
                            @endif
                            <a class="btn btn-secondary" style="color:white;" href="{{ route('event.delete',['id'=>$event->id]) }}">
                                {{ $string['delete'] }}
                            </a>
                        @endif
                    </div>
                    <div class="col-md-2 col-xs-1" style="text-align: left; margin-top: 100px;">
                        <a href="{{ route('event.single',['id'=>$next_event_id]) }}"><img src="/images/next.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection