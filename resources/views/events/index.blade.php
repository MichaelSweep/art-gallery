@extends('layouts.app')

@section('meta_tags')
    <title>
        Amacyprus - {{ $string['events'] }}
    </title>
@endsection

@section('content')

    <div class="container">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel mt-md-5">
            <div class="container">
                <ul class="nav navbar-nav row">

                    @if(Auth::check())
                        @if(Auth::user()->admin)
                            <li class="nav-item">
                                <a class="nav-link btn btn-light mr-1" role="button"
                                   href="{{ route('event.create.form') }}">{{ $string['add_event'] }}</a>
                            </li>
                        @endif
                    @endif

                    <li class="nav-item">
                            <a class="nav-link btn btn-light" role="button"
                               href="{{ route('events.calendar') }}">{{ $string['view as'] }}: {{ $string['calendar'] }}</a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link btn btn-light" role="button"
                               href="{{ route('events') }}">{{ $string['view as'] }}: {{ $string['chronologically'] }}</a>
                    </li>

                </ul>
            </div>
        </nav>

        <div class="row content-row">
            @foreach($events as $event)
                <div class="col-md-12 event-card">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-3 date">
                            <p>{{ Carbon\Carbon::parse($event->date_start)->format('M d, Y') }}</p>
                        </div>
                        <div class="col-md-3 description">
                            <a href="{{ route('event.single',['id'=>$event->id]) }}">
                                <img src="{{ $event->image }}" alt="" class="event-img">
                            </a>
                            <a href="{{ route('event.single',['id'=>$event->id]) }}">
                                <strong>{{ $event->name }}</strong>
                            </a>
                            <p>
                                {{ Carbon\Carbon::parse($event->date_start)->format('M d, Y') }} - {{ Carbon\Carbon::parse($event->date_fin)->format('M d, Y') }}
                            </p>
                            <p>
                                {{ $event->place }}, {{ $event->city }}, {{ $event->country }}
                            </p>
                            @if(!$event->activated)
                                <h3 style="color:red">
                                    EVENT DEACTIVATED
                                </h3>
                            @endif
                            @if(Auth::check() && Auth::user()->admin)
                                <a class="btn btn-secondary" style="color:white;" href="{{ route('event.update.form',['id'=>$event->id]) }}">
                                    {{ $string['edit'] }}
                                </a>
                                @if($event->activated)
                                    <a class="btn btn-secondary" style="color:white;" href="{{ route('event.deactivate',['id'=>$event->id]) }}">
                                        {{ $string['deactivate'] }}
                                    </a>
                                @else
                                    <a class="btn btn-secondary" style="color:white;" href="{{ route('event.activate',['id'=>$event->id]) }}">
                                        {{ $string['activate'] }}
                                    </a>
                                @endif
                                <a class="btn btn-secondary" style="color:white;" href="{{ route('event.delete',['id'=>$event->id]) }}">
                                    {{ $string['delete'] }}
                                </a>
                            @endif
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            @endforeach
        </div>
        {!! $events->appends(\Request::except('page'))->render() !!}
    </div>
@endsection