@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $string['status'] }}</div>
                    <div class="card-body">
                        <form action="{{ route('artist.delete', ['id'=>$user_id]) }}" method="GET">
                            <input type="hidden" name="confirmed">
                            <div class="row">
                                <h2 class="col-md-6 mx-auto text-lg-center card-title">
                                    This artist will be permanently DELETED
                                </h2>
                                <h3 class="col-md-6 mx-auto text-lg-center card-title">
                                    Paintings of this Artist will be permanently DELETED
                                    Try to use DEACTIVATE instead. No one except you can see this Artist and his paintings after deactivation.
                                </h3>
                            </div>

                            <div class="row">
                                <a href="{{ URL::previous() }}" class="col-md-4 mx-auto btn btn-dark">{{ $string['cancel'] }}</a>
                                <a onclick="$(this).closest('form').submit()" class="col-md-4 mx-auto btn btn-dark" style="color: white">{{ $string['continue'] }}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection