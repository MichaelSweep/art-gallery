@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center content-row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $string['dashboard'] }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{ $string['you_are_logged_in'] }}!
                </div>
                <a href="/" type="button" class="btn btn-light btn-block">{{ $string['home_page'] }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
