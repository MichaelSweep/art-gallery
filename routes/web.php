<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Site pages
Route::group([], function ()
{
    Route::get('/locale/{id}', ['uses' => 'SiteController@changeLocale', 'as' => 'site.change.locale']);
    Route::get('/map', ['uses' => 'SiteController@map', 'as' => 'site.map']);
    Route::get('/management', ['uses' => 'SiteController@management', 'as' => 'site.management']);
    Route::get('/info', ['uses' => 'SiteController@info', 'as' => 'site.info']);
    Route::get('/home', ['uses' => 'HomeController@index', 'as' => 'site.home']);
    Route::post('email/send', ['uses' => 'MailController@send', 'as' => 'site.email.send']);
    Route::get('email/send/{id}', ['uses' => 'MailController@emailWithDrawing', 'as' => 'site.email.send.drawing']);
    Route::get('email/send', ['uses' => 'MailController@email', 'as' => 'site.email']);
});

// Events
Route::group([], function ()
{
    Route::get('events', ['as' => 'events', 'uses' => 'EventController@index']);
    Route::get('events/calendar', ['as' => 'events.calendar', 'uses' => 'EventController@calendar']);

    Route::get('event/create', ['as' => 'event.create.form', 'uses' => 'EventController@createForm']);
    Route::post('event/create', ['as' => 'event.create', 'uses' => 'EventController@create']);

    Route::get('event/update/{id}', ['as' => 'event.update.form', 'uses' => 'EventController@updateForm']);
    Route::post('event/update', ['as' => 'event.update', 'uses' => 'EventController@update']);

    Route::get('event/activate/{id}', ['as' => 'event.activate', 'uses' => 'EventController@activate']);
    Route::get('event/deactivate/{id}', ['as' => 'event.deactivate', 'uses' => 'EventController@deactivate']);
    Route::get('event/delete/{id}', ['as' => 'event.delete', 'uses' => 'EventController@delete']);

    Route::get('event/remove/{id}', ['as' => 'event.remove', 'uses' => 'EventController@remove']);
    Route::get('event/{id}', ['as' => 'event.single', 'uses' => 'EventController@single']);
});

// Drawings
Route::group([], function ()
{
    Route::get('/', ['as' => 'drawings', 'uses' => 'DrawingController@index']);
    Route::get('drawing/find', ['as' => 'drawing.find', 'uses' => 'DrawingController@find']);

    Route::get('drawing/update/{id}',['as' => 'drawing.update.form', 'uses' => 'DrawingController@updateForm']);
    Route::post('drawing/update',['as' => 'drawing.update','uses' => 'DrawingController@update']);

    Route::get('drawing/remove/{id}',['as' => 'drawing.remove.form', 'uses' => 'DrawingController@remove']);
    Route::get('drawing/activate/{id}',['as' => 'drawing.activate', 'uses' => 'DrawingController@activate']);
    Route::get('drawing/delete/{id}',['as' => 'drawing.delete', 'uses' => 'DrawingController@delete']);

    Route::get('drawing/create',['as' => 'drawing.create.form', 'uses' => 'DrawingController@createForm']);
    Route::post('drawing/create',['as' => 'drawing.create','uses' => 'DrawingController@create']);

    Route::get('image/{id}',['as' => 'drawing.single', 'uses' => 'DrawingController@image']);
});

// Artists
Route::group([], function ()
{
    Route::get('artists', ['as' => 'artists', 'uses' => 'UserController@index']);
    Route::get('artist/{artistId}', ['as' => 'artist.single', 'uses' => 'UserController@single']);

    Route::get('artist/update/{id}', ['as' => 'artist.update.form', 'uses' => 'UserController@updateForm']);
    Route::post('artist/update', ['as' => 'artist.update', 'uses' => 'UserController@update']);

    Route::get('artist/remove/{id}', ['as' => 'artist.remove', 'uses' => 'UserController@remove']);
    Route::get('artist/delete/{id}', ['as' => 'artist.delete', 'uses' => 'UserController@delete']);

    Route::get('artist/addAvatar/{id}', ['as' => 'artist.add.avatar.form', 'uses' => 'UserController@addAvatarForm']);
    Route::post('artist/addAvatar', ['as' => 'artist.add.avatar', 'uses' => 'UserController@addAvatar']);

    Route::get('artist/activate', ['as' => 'artist.activate.form', 'uses' => 'UserController@activateForm']);
    Route::get('artist/activate/{id}', ['as' => 'artist.activate', 'uses' => 'UserController@activate']);

    Route::get('artist/setRole/{id}', ['as' => 'artist.set.role.form', 'uses' => 'UserController@setRoleForm']);
    Route::post('artist/setRole', ['as' => 'artist.set.role', 'uses' => 'UserController@setRole']);
});

// Admin
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function ()
{
    Route::get('/', ['as' => 'admin.index', 'uses' => 'Admin\HomeController@index']);
    Route::get('artists/', ['as' => 'admin.artists', 'uses' => 'Admin\UserController@index']);
    Route::get('artist/update/{id}', ['as' => 'admin.artist.update.form', 'uses' => 'Admin\UserController@updateForm']);
    Route::post('artist/update', ['as' => 'admin.artist.update', 'uses' => 'Admin\UserController@update']);

    Route::get('roles/', ['as' => 'admin.roles', 'uses' => 'Admin\RoleController@index']);
    Route::get('role/update/{id}', ['as' => 'admin.role.update.form', 'uses' => 'Admin\RoleController@updateForm']);
    Route::post('role/update', ['as' => 'admin.role.update', 'uses' => 'Admin\RoleController@update']);
    Route::post('role/create', ['as' => 'admin.role.create', 'uses' => 'Admin\RoleController@create']);
    Route::post('role/remove/{id}', ['as' => 'admin.role.remove', 'uses' => 'Admin\RoleController@remove']);

    Route::get('translations/', ['as' => 'admin.translations', 'uses' => 'Admin\TranslationController@index']);
    Route::get('translation/update/{id}', ['as' => 'admin.translation.update.form', 'uses' => 'Admin\TranslationController@updateForm']);
    Route::post('translation/update', ['as' => 'admin.translation.update', 'uses' => 'Admin\TranslationController@update']);
    Route::post('translation/create', ['as' => 'admin.translation.create', 'uses' => 'Admin\TranslationController@create']);
    Route::get('translation/remove/{id}', ['as' => 'admin.translation.remove', 'uses' => 'Admin\TranslationController@remove']);

    Route::get('languages/', ['as' => 'admin.languages', 'uses' => 'Admin\LanguageController@index']);
    Route::get('language/update/{id}', ['as' => 'admin.language.update.form', 'uses' => 'Admin\LanguageController@updateForm']);
    Route::post('language/update', ['as' => 'admin.language.update', 'uses' => 'Admin\LanguageController@update']);
    Route::post('language/create', ['as' => 'admin.language.create', 'uses' => 'Admin\LanguageController@create']);
    Route::get('language/remove/{id}', ['as' => 'admin.language.remove', 'uses' => 'Admin\LanguageController@remove']);

    Route::get('drawings/', ['as' => 'admin.drawings', 'uses' => 'Admin\DrawingController@index']);
    Route::get('drawing/update/{id}', ['as' => 'admin.drawing.update.form', 'uses' => 'Admin\DrawingController@updateForm']);
    Route::post('drawing/update', ['as' => 'admin.drawing.update', 'uses' => 'Admin\DrawingController@update']);
    Route::post('drawing/create', ['as' => 'admin.drawing.create', 'uses' => 'Admin\DrawingController@create']);
    Route::get('drawing/remove/{id}', ['as' => 'admin.drawing.remove', 'uses' => 'Admin\DrawingController@remove']);
    Route::get('drawing/create', ['as' => 'admin.drawing.create.form', 'uses' => 'Admin\DrawingController@createForm']);

    Route::get('events/', ['as' => 'admin.events', 'uses' => 'Admin\EventController@index']);
    Route::get('event/update/{id}', ['as' => 'admin.event.update.form', 'uses' => 'Admin\EventController@updateForm']);
    Route::post('event/update', ['as' => 'admin.event.update', 'uses' => 'Admin\EventController@update']);
    Route::post('event/create', ['as' => 'admin.event.create', 'uses' => 'Admin\EventController@create']);
    Route::get('event/remove/{id}', ['as' => 'admin.event.remove', 'uses' => 'Admin\EventController@remove']);
    Route::get('event/create', ['as' => 'admin.event.create.form', 'uses' => 'Admin\EventController@createForm']);
});

Route::get('/test', function ()
{
});