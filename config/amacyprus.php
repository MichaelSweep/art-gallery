<?php

return [
    'default_language' => 2,

    'users_per_page'    => 15,
    'drawings_per_page' => 15,
    'events_per_page'   => 15,

    // admin items
    'translations_per_page'   => 30,
    'languages_per_page'   => 15,
    'admin_drawings_per_page'   => 30,
    'admin_events_per_page'   => 30,
];