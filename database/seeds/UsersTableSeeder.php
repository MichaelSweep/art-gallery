<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'admin' => 1,
            'activated' => 1,
            'city' => 'Las Vegas',
            'country' => 'USA',
            'bio' => 'I am admin'
        ]);

        User::create([
            'name' => 'user',
            'email' => 'user@user.com',
            'password' => bcrypt('user'),
            'admin' => 0,
            'activated' => 1,
            'city' => 'Las Vegas',
            'country' => 'USA',
            'bio' => 'I am user'
        ]);
    }
}
