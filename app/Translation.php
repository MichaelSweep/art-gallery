<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable = [
        'code', 'language_id', 'content'
    ];

    public function language()
    {
        return $this->belongsTo('App\Language');
    }
}