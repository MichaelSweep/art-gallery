<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Drawing extends Model
{
    use Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'artistId', 'picture', 'activated', 'redacted', 'title', 'city', 'country',
        'date', 'genre', 'technology', 'size', 'status', 'price', 'description'
    ];

    public $sortable = ['title', 'created_at', 'updated_at', 'price', 'date', 'genre'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Deactivate drawing
     *
     * @return bool
     */
    public function deactivate()
    {
        return $this->update(['activated' => 0]);
    }

    /**
     * Activate drawing
     *
     * @return bool
     */
    public function activate()
    {
        return $this->update(['activated' => 1]);
    }

    /**
     * Get artist of drawing
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function getArtist()
    {
        return $this->belongsTo('App\User','artistId');
    }
}
