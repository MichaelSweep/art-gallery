<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable
{
    use Notifiable, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'country', 'bio', 'email', 'password', 'avatar', 'activated', 'admin', 'role_id', 'language_id', 'position'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'password', 'remember_token', 'admin', 'role'
    ];

    /**
     * The attributes that are sortable.
     *
     * @var array
     */
    protected $sortable = ['name', 'created_at'];

    /**
     * Indicates users as admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * Has many relations with Drawing: User has many Drawings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function drawings()
    {
        return $this->hasMany('App\Drawing', 'artistId');
    }

    /**
     * Deactivate user
     *
     * @return bool
     */
    public function deactivate()
    {
        return $this->update(['activated' => 0]);
    }

    /**
     * Activate user
     *
     * @return bool
     */
    public function activate()
    {
        return $this->update(['activated' => 1]);
    }

    /**
     * Activate user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function getRole()
    {
        return $this->belongsTo('App\Role','role_id');
    }

    public function scopeIsActivated($query)
    {
        return $query->where('activated',1);
    }

    public function scopeShowInArtists($query)
    {
        if($this->role_id != null)
        {
            return $query->whereHas('getRole', function ($query) {
                $query->where('show_in_artists', 1);
            });
        }
        else {
            return $query;
        }

    }
}