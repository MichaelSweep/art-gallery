<?php namespace App\Http\ViewComposers;

use \DB;
use \Illuminate\Contracts\View\View;
use App\Translation;
use Illuminate\Support\Facades\Auth;

class TranslationComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::check())
        {
            $language_id = Auth::user()->language_id;
        }
        else
        {
            $language_id = config('amacyprus.default_language');
        }
        $translations = Translation::where('language_id',$language_id)->pluck('content','code');

        $view->with('string', $translations);
    }
}
