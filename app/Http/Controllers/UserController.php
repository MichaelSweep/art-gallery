<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Create a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['single', 'index', 'activate', 'activateForm' , 'findArtist']]);
        $this->middleware('admin', ['only' => ['activate', 'getSetRole', 'setRole']]);
    }

    public function index()
    {
        session()->reflash();
        if(isset($_GET['sort']))
        {
            if(isset($_GET['search']))
            {
                $term = $_GET['search'];
                $search = "%{$term}%";
                $artists = App\User::query()->sortable()->where('name','like',$search)->paginate(config('amacyprus.users_per_page'));
            }
            else
            {
                $artists = App\User::query()->sortable()->paginate(config('amacyprus.users_per_page'));
            }
        }
        else
        {
            if(isset($_GET['search']))
            {
                $term = $_GET['search'];
                $search = "%{$term}%";
                $artists = App\User::query()->sortable()->where('name','like',$search)->paginate(config('amacyprus.users_per_page'));
            }
            else
            {
                if(!session()->has('random_users'))
                {
                    $random_ids = User::orderByRaw('RAND()')
                        ->pluck('id')
                        ->toArray();

                    $ids_ordered = implode(',', $random_ids);
                    session()->flash('random_users', $ids_ordered);
                    session()->flash('random_users_object', $random_ids);
                }

                $ids_ordered = session()->get('random_users');

                $artists = User::orderByRaw(DB::raw("FIELD (id, $ids_ordered) DESC"))->sortable()->paginate(config('amacyprus.users_per_page'));
            }
        }

        if(isset($term))
        {
            return view('user.index', compact(['artists','term']));
        }
        else
        {
            //return $artists;
            //return session()->get('random_users');
            //return view();
            return view('user.index', compact(['artists']));
        }
    }

    public function single($id)
    {
        $artist = User::find($id);

        return view('user.userPage', compact('artist'));
    }

    public function activateForm()
    {
        $artists = App\User::where('activated', '=', '0')->get();

        return view('user.activate', compact('artists'));
    }

    public function activate($id)
    {
        $user = User::find($id);
        $user->activate();

	    $drawings = $user->drawings()->get();
	    foreach ($drawings as $drawing)
	    {
		    $drawing->activate(); // deactivate every picture in db
	    }

/*        $message = ['text' => 'User activation', 'status' => 'DONE'];
        return view('status', compact('message'));*/

        return back();
    }

    public function updateForm($id)
    {
        $artist = User::find($id);

        return view('user.update', compact('artist'));
    }

    public function update(Request $request)
    {
        $user = App\User::find($request->id);

        $user->name = $request->name;
        $user->country = $request->country;
        $user->bio = $request->bio;
        $user->position = $request->position;

        $user->save();

        $artist = User::find($request->id);

        return view('user.userPage', compact('artist'));
    }

    public function remove($id)
    {
        // Admin can't delete himself || User can delete himself
        if((Auth::user()->isAdmin() && Auth::id() != $id) || (!Auth::user()->isAdmin() && Auth::id() == $id))
        {
            $user = App\User::find($id);
            $user->deactivate();

            $drawings = $user->drawings()->get();
            foreach ($drawings as $drawing)
            {
                $drawing->deactivate(); // deactivate every picture in db
            }

            return back();
        }
        else
        {
            return back();
        }
    }

    public function addAvatarForm($id)
    {
        $artist = User::find($id);

        return view('user.addAvatar', compact('artist'));
    }

    public function addAvatar(Request $request)
    {
        $user = App\User::find($request->id);
        $this->validate($request, ['image' => 'required|image|max:6000']);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . '_' . $file->getClientOriginalName();
            $file->move(storage_path('/images'), $name);
            $user->avatar = 'storage/images/' . $name;
            $user->save();
        }
        $message = ['text' => 'Avatar adding', 'status' => 'DONE'];

        return view('status', compact('message'));
    }

    public function setRoleForm($id)
    {
        $artist = App\User::find($id);

        return view('user.setRole', compact('artist'));
    }

    public function setRole(Request $request)
    {
        $user = App\User::find($request->id);
        $user->role = $request->role;
        $user->save();
        $message = ['text' => 'Role set', 'status' => 'DONE'];

        return view('status', compact('message'));
    }

    public function delete($id)
    {
        if(isset($_GET['confirmed']))
        {
            // Admin can't delete himself || User can delete himself
            if((Auth::user()->isAdmin() && Auth::id() != $id) || (!Auth::user()->isAdmin() && Auth::id() == $id))
            {
                $user = App\User::find($id);
                if(file_exists($user->avatar))
                unlink($user->avatar); // delete user avatar

                $drawings = $user->drawings()->get();
                foreach ($drawings as $drawing)
                {
                    if(file_exists($drawing->picture))
                    unlink($drawing->picture); // delete every picture from storage
                    $drawing->delete();
                    //$drawing->deactivate(); // deactivate every picture in db
                }
                $user->delete();

                return redirect()->route('artists');
            }
            else
            {
                return redirect()->route('artists');
            }
        }
        else
        {
            return view('confirm_deleting')->with('user_id',$id);
        }
    }

    // public function
}
