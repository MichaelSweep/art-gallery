<?php

namespace App\Http\Controllers\Admin;

use App;
use Illuminate\Http\Request;
use App\Role;
use App\Event;
use App\Language;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Create a new EventController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        if(isset($_GET['search']))
        {
            $term = $_GET['search'];
            $events = Event::where('code', 'like' , '%'.$term.'%')
                ->orWhere('content', 'like', '%'.$term.'%')
                ->orderBy('id','desc')
                ->paginate(config('amacyprus.admin_events_per_page'));

            return view('admin.pages.events',compact(['events', 'term']));
        }
        else
        {
            $events = Event::orderBy('id','desc')->paginate(config('amacyprus.admin_events_per_page'));

            return view('admin.pages.events',compact(['events']));
        }
    }

    public function createForm()
    {
        return view('admin.pages.event_create');
    }

    public function updateForm($id)
    {
        $event = Event::find($id);

        return view('admin.pages.event',compact(['event']));
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $event_id = $data['event_id'];
        $event = Event::find($event_id);
        $this->validate($request, ['image' => 'image']);
        if(isset($data['activated']))
        {
            $data['activated'] = 1;
        }
        else
        {
            $data['activated'] = 0;
        }
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . '_' . $file->getClientOriginalName();
            $file->move(storage_path('images/'), $name);
            if(file_exists($event->picture))
            {
                unlink($event->picture);
            }
            $data['image'] = 'storage/images/' . $name;
        }
        $event->update($data);

        return back();
    }

    public function create(Request $request)
    {
        $event = new App\Event();
        $data = $request->all();
        $this->validate($request, ['image' => 'image']);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . '_' . $file->getClientOriginalName();
            $file->move(storage_path('images/'), $name);
            $data['image'] = 'storage/images/' . $name;
        }
        $event->create($data);

        return back();
    }

    public function remove($id)
    {
        Event::find($id)->delete();

        return back();
    }

    public function single($id)
    {
        $event = Event::find($id);

        return view('events.single',compact('event'));
    }
}
