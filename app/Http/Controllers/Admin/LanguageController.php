<?php

namespace App\Http\Controllers\Admin;

use App;
use Illuminate\Http\Request;
use App\Language;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    /**
     * Create a new LanguageController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        $languages = Language::orderBy('id','desc')->paginate(config('amacyprus.users_per_page'));

        return view('admin.pages.languages',compact('languages'));
    }

    public function updateForm($id)
    {
        $language = Language::find($id);

        return view('admin.pages.language',compact('language'));
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $language_id = $data['language_id'];
        $role = Language::find($language_id);
        $role->update($data);

        return back();
    }

    public function create(Request $request)
    {
        $data = $request->all();
        Language::create($data);

        return back();
    }

    public function remove($id)
    {
        Language::find($id)->delete();

        return back();
    }
}
