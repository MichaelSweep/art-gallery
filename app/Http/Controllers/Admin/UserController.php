<?php

namespace App\Http\Controllers\Admin;

use App;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Create a new AdminController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        $artists = User::query()->paginate(config('amacyprus.users_per_page'));


        return view('admin.pages.artists',compact(['artists']));
    }

    public function updateForm($id)
    {
        $artist = User::find($id);
        $roles = Role::all();

        return view('admin.pages.artist',compact(['artist','roles']));
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $artist_id = $data['artist_id'];
        $user = User::find($artist_id);
        $this->validate($request, ['image' => 'image']);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . '_' . $file->getClientOriginalName();
            $file->move(storage_path('/images'), $name);
            $user->avatar = 'storage/images/' . $name;
            $user->save();
        }
        $user->update($data);

        return back();
    }
}
