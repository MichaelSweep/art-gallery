<?php

namespace App\Http\Controllers\Admin;

use App;
use Illuminate\Http\Request;
use App\Role;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Create a new AdminController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        $roles = Role::orderBy('id','desc')->paginate(config('amacyprus.users_per_page'));

        return view('admin.pages.roles',compact('roles'));
    }

    public function updateForm($id)
    {
        $role = Role::find($id);

        return view('admin.pages.role',compact('role'));
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $role_id = $data['role_id'];
        $role = Role::find($role_id);
        $this->validate($request, ['title' => 'required']);
        $role->update($data);

        return back();
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $this->validate($request, ['title' => 'required']);
        Role::create($data);

        return back();
    }

    public function remove(Request $request)
    {
        $data = $request->all();
        $role_id = $data['role_id'];
        Role::find($role_id)->delete();

        return back();
    }
}
