<?php

namespace App\Http\Controllers\Admin;

use App;
use Illuminate\Http\Request;
use App\User;
use App\Event;
use App\Drawing;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new AdminController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        $users_count = User::all()->count();
        $events_count = Event::all()->count();
        $drawings_count = Drawing::all()->count();

        return view('admin.index',compact(['users_count','events_count','drawings_count']));
    }
}
