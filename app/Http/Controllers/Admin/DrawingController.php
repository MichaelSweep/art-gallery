<?php

namespace App\Http\Controllers\Admin;

use App;
use Illuminate\Http\Request;
use App\Role;
use App\Drawing;
use App\Language;
use App\Http\Controllers\Controller;

class DrawingController extends Controller
{
    /**
     * Create a new DrawingController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        if(isset($_GET['search']))
        {
            $term = $_GET['search'];
            $drawings = Drawing::where('code', 'like' , '%'.$term.'%')
                ->orWhere('content', 'like', '%'.$term.'%')
                ->orderBy('id','desc')
                ->paginate(config('amacyprus.admin_drawings_per_page'));

            return view('admin.pages.drawings',compact(['drawings', 'term']));
        }
        else
        {
            $drawings = Drawing::orderBy('id','desc')->paginate(config('amacyprus.admin_drawings_per_page'));

            return view('admin.pages.drawings',compact(['drawings']));
        }
    }

    public function createForm()
    {
        return view('admin.pages.drawing_create');
    }

    public function updateForm($id)
    {
        $drawing = Drawing::find($id);

        return view('admin.pages.drawing',compact(['drawing']));
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $drawing_id = $data['drawing_id'];
        $drawing = Drawing::find($drawing_id);
        $data['size'] = $request->sizeOne . ' X ' . $request->sizeTwo . ' ' . $request->unit;
        $this->validate($request, ['image' => 'image']);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . '_' . $file->getClientOriginalName();
            $file->move(storage_path('images/'), $name);
            if(file_exists($drawing->picture))
            {
                unlink($drawing->picture);
            }
            $data['picture'] = 'storage/images/' . $name;
	        image_fix_orientation($data['picture']);
        }
        $drawing->update($data);

        return back();
    }

    public function create(Request $request)
    {
        $drawing = new App\Drawing();
        $data = $request->all();
        $data['size'] = $request->sizeOne . ' X ' . $request->sizeTwo . ' ' . $request->unit;
        $this->validate($request, ['image' => 'image']);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . '_' . $file->getClientOriginalName();
            $file->move(storage_path('images/'), $name);
            $data['picture'] = 'storage/images/' . $name;
	        image_fix_orientation($data['picture']);
        }
        $drawing->create($data);

        return back();
    }

    public function remove($id)
    {
        Drawing::find($id)->delete();

        return back();
    }
}
