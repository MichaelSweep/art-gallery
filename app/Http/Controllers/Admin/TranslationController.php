<?php

namespace App\Http\Controllers\Admin;

use App;
use Illuminate\Http\Request;
use App\Role;
use App\Translation;
use App\Language;
use App\Http\Controllers\Controller;

class TranslationController extends Controller
{
    /**
     * Create a new TranslationController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        $languages = Language::all();
        if(isset($_GET['search']))
        {
            $term = $_GET['search'];
            $translations = Translation::where('code', 'like' , '%'.$term.'%')
                ->orWhere('content', 'like', '%'.$term.'%')
                ->orderBy('id','desc')
                ->paginate(config('amacyprus.translations_per_page'));

            return view('admin.pages.translations',compact(['translations','languages', 'term']));
        }
        else
        {
            $translations = Translation::orderBy('id','desc')->paginate(config('amacyprus.translations_per_page'));

            return view('admin.pages.translations',compact(['translations','languages']));
        }
    }

    public function updateForm($id)
    {
        $translation = Translation::find($id);
        $languages = Language::all();

        return view('admin.pages.translation',compact(['translation','languages']));
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $translation_id = $data['translation_id'];
        $translation = Translation::find($translation_id);
        $translation->update($data);

        return back();
    }

    public function create(Request $request)
    {
        $data = $request->all();
        Translation::create($data);

        return back();
    }

    public function remove($id)
    {
        Translation::find($id)->delete();

        return back();
    }
}
