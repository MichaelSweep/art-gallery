<?php

namespace App\Http\Controllers;

use App\Drawing;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Validator;

class MailController extends Controller
{
    /**
     * Create a new MailController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    public function send()
    {
        $this->validator(Request::all())->validate();
        Mail::send(new sendMail());
        $message = ['text' => 'E-mail sending', 'status' => 'DONE'];
        return view('status', compact('message'));
    }

    public function email()
    {
        return view('mail.email');
    }

    public function emailWithDrawing($id)
    {
        $drawing = Drawing::find($id);

        return view('mail.emailWithDrawing', compact('drawing'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'subject' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'message' => 'required|string',
            'captcha' => 'required|captcha',
        ]);
    }
}
