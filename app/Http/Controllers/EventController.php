<?php

namespace App\Http\Controllers;

use App;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;

class EventController extends Controller
{
    /**
     * Create a new EventController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'single' ,'calendar']]);
    }

    /**
     * */
    public function index()
    {
    	if(Auth::check() && Auth::user()->admin)
	    {
		    $events = App\Event::orderBy('date_start','desc')->paginate(config('amacyprus.events_per_page'));
	    }
	    else
	    {
		    $events = App\Event::where('activated',1)->orderBy('date_start','desc')->paginate(config('amacyprus.events_per_page'));
	    }

        return view('events.index', compact('events'));
    }

    public function calendar()
    {
        $calendar_event = [];

        $events = \App\Event::where('activated',1)->get(); //EventModel implements MaddHatter\LaravelFullcalendar\Event

	    foreach ($events as $event)
	    {
	    	$calendar_event[] = \Calendar::event(
			    $event->name, //event title
			    true, //full day event?
			    new \DateTime($event->date_start), //start time (you can also use Carbon instead of DateTime)
			    new \DateTime($event->date_fin), //end time (you can also use Carbon instead of DateTime)
			    1,
			    [
				    'url' => route('event.single',['id'=>$event->id]),
			    ]
		    );
	    }

	    $calendar = \Calendar::addEvents($calendar_event);

        return view('events.calendar',compact('calendar'));
    }

    public function createForm()
    {
        return view('events.create');
    }

    public function create(Request $request)
    {

	    $event = new App\Event();
	    $data = $request->all();
	    $this->validate($request, ['image' => 'image']);
	    if ($request->hasFile('image')) {
		    $file = $request->file('image');
		    $name = time() . '_' . $file->getClientOriginalName();
		    $file->move(storage_path('images/'), $name);
		    $data['image'] = 'storage/images/' . $name;
	    }
	    $event->create($data);

	    return redirect()->route('events');
    }

    public function updateForm($id)
    {
        $event = App\Event::find($id);
        return view('events.update', compact('event'));
    }

    public function update(Request $request)
    {
        $event = App\Event::find($request->id);

        $event->name = $request->name;
        $event->city = $request->city;
        $event->country = $request->country;
        $event->date = $request->date;
        $event->description = $request->description;

        $event->save();
        $message = ['text' => 'Drawing creating', 'status' => 'DONE'];
        return view('status', compact('message'));
    }

	public function single($id)
	{
		$event = App\Event::find($id);
		$i = 0;
		$j = 0;
		$first_event = App\Event::where('activated',1)->orderBy('date_start','asc')->first();
		$last_event = App\Event::where('activated',1)->orderBy('date_start','asc')->get()->last();

		do {
			$i += 1;
			if($last_event->id != $id)
			{
				// drawing has next image
				$next_event = App\Event::where('activated',1)->where('date_start','>',$event->date_start)->first();
			}
			else
			{
				// drawing hasn't next event, so next image is current
				$next_event = App\Event::find($id);
			}
		} while($next_event == null || !$next_event->activated);

		do {
			$j += 1;
			if($id != $first_event->id)
			{
				$prev_event = App\Event::where('activated',1)->where('date_start','<',$event->date_start)->first();
			}
			else
			{
				// if there are no previous images
				$prev_event = App\Event::find($id);
			}
		} while($prev_event == null || !$prev_event->activated);

		$next_event_id = $next_event->id;
		$prev_event_id = $prev_event->id;

		return view('events.single', compact(['event','next_event_id','prev_event_id']));
	}

    public function remove($id)
    {
        App\Event::find($id)->deactivate();

        return back();
    }

	public function deactivate($id)
	{
		App\Event::find($id)->deactivate();

		return back();
	}

	public function activate($id)
	{
		App\Event::find($id)->activate();

		return back();
	}

	public function delete($id)
	{
		if(isset($_GET['confirmed']))
		{
			$event = Event::find($id);
			if(Auth::user()->isAdmin())
			{
				if(file_exists($event->image))
					unlink($event->image); // delete every picture from storage
				$event->delete();

				return redirect()->route('events');
			}
			else
			{
				return redirect()->route('events');
			}
		}
		else
		{
			return view('confirm_deleting_event')->with('event_id',$id);
		}
	}

}
