<?php

namespace App\Http\Controllers;

use App\Drawing;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mail;
use App\mail\SendMail;
use App\User;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    /**
     * Create a new SiteController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function map()
    {
        return view('map');
    }

    public function management()
    {
        $artists = User::all();

        return view('user.managers', compact('artists'));
    }

    public function info()
    {
        return view('info');
    }

    public function changeLocale($id)
    {
        if(Auth::check())
        {
            Auth::user()->update(['language_id'=>$id]);
        }

        return back();
    }
}