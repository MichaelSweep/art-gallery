<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\Drawing;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DrawingController extends Controller
{
    /**
     * Create a new DrawingController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index', 'find', 'image']]);
    }

    public function index()
    {
        session()->reflash();
        if(isset($_GET['sort']))
        {
            $drawings = App\Drawing::sortable()->paginate(config('amacyprus.drawings_per_page'));
        }
        else
        {
            if(!session()->has('random_drawings'))
            {
                $random_ids = Drawing::orderByRaw('RAND()')
                    ->pluck('id')
                    ->toArray();

                $ids_ordered = implode(',', $random_ids);
                session()->flash('random_drawings', $ids_ordered);
            }

            $ids_ordered = session()->get('random_drawings');

            $drawings = Drawing::sortable()->orderByRaw(DB::raw("FIELD (id, $ids_ordered) DESC"))->paginate(config('amacyprus.users_per_page'));
        }

        return view('welcome', compact('drawings'));
    }

    public function createForm()
    {
        return view('draw.create');
    }

    public function create(Request $request)
    {
        $drawing = new App\Drawing();
        $artistId = \Auth::user()->id;

        $drawing->artistId = $artistId;
        $drawing->title = $request->title;
        $drawing->city = $request->city;
        $drawing->country = $request->country;
        $drawing->date = $request->date;
        $drawing->genre = $request->genre;
        $drawing->technology = $request->technology;
        $drawing->size = $request->sizeOne . ' X ' . $request->sizeTwo . ' ' . $request->unit;
        $drawing->status = $request->status;
        $drawing->price = $request->price;
        $drawing->description = $request->description;
        $this->validate($request, ['image' => 'image|max:6000']);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . '_' . $file->getClientOriginalName();
            $file->move(storage_path('images/'), $name);
            $drawing->picture = 'storage/images/' . $name;
            image_fix_orientation($drawing->picture);
        }

        $drawing->save();
        $message = ['text' => 'Drawing creating', 'status' => 'DONE'];

        return view('status', compact('message'));
    }

    public function updateForm($id)
    {
        $drawing = App\Drawing::find($id);

        return view('draw.update', compact('drawing'));
    }

    public function update(Request $request)
    {
        $drawing = App\Drawing::find($request->id);

        $drawing->title = $request->title;
        $drawing->city = $request->city;
        $drawing->country = $request->country;
        $drawing->date = $request->date;
        $drawing->genre = $request->genre;
        $drawing->technology = $request->technology;
        $drawing->size = $request->sizeOne . ' X ' . $request->sizeTwo . ' ' . $request->unit;
        $drawing->status = $request->status;
        $drawing->price = $request->price;
        $drawing->description = $request->description;

        $drawing->save();
        $message = ['text' => 'Drawing updating', 'status' => 'DONE'];

        return view('status', compact('message'));
    }

    public function remove($id)
    {
        App\Drawing::find($id)->deactivate();

        return back();
    }

    public function activate($id)
    {
	    App\Drawing::find($id)->activate();

	    return back();
    }

	public function delete($id)
	{
		if(isset($_GET['confirmed']))
		{
			$drawing = Drawing::find($id);
			// Admin can't delete himself || User can delete himself
			if(Auth::user()->isAdmin())
			{
				if(file_exists($drawing->picture))
					unlink($drawing->picture); // delete every picture from storage
				$drawing->delete();

				return redirect()->route('drawings');
			}
			else
			{
				return redirect()->route('drawings');
			}
		}
		else
		{
			return view('confirm_deleting_drawing')->with('drawing_id',$id);
		}
	}

    public function find(Request $request)
    {
        $term = $request->search;
        $search = "%{$term}%";
        $drawings = App\Drawing::sortable()->where('title', 'like', $search)->paginate(config('amacyprus.drawings_per_page'));

        return view('welcome', compact(['drawings', 'term']));
    }

    public function image($id)
    {
        $drawing = App\Drawing::find($id);
        $i = 0;
        $j = 0;
        $first_drawing = App\Drawing::where('activated',1)->first();
        $last_drawing = App\Drawing::where('activated',1)->get()->last();
        do {
        	$i += 1;
        	if($last_drawing->id != $id)
	        {
	        	// drawing has next image
		        $next_image = App\Drawing::find($id+$i);
	        }
	        else
	        {
	        	// drawing hasn't next image, so next image is current
		        $next_image = App\Drawing::find($id);
	        }
        } while($next_image == null || ($next_image != null && !$next_image->activated));

	    do {
		    $j += 1;
		    if($id != $first_drawing->id)
		    {
			    $prev_image = App\Drawing::find($id-$j);
		    }
		    else
		    {
		    	// if there are no previous images
			    $prev_image = App\Drawing::find($id);
		    }
	    } while($prev_image == null || ($prev_image != null && !$prev_image->activated));

	    $next_image_id = $next_image->id;
	    $prev_image_id = $prev_image->id;

        return view('draw.index', compact(['drawing', 'next_image_id', 'prev_image_id']));
    }
}
