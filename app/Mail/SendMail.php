<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        return $this->view('mail.mail',['msg'=>$request->message])
            ->replyTo($request->email)
            ->from("info@amacyprus.org")
            ->to("amacyprusinfo@gmail.com")
            ->subject('Feedback from amacyprus.org with subject "' .$request->subject . '"');
    }
}
