<?php

function limit_text($string, $word_limit) {
    $string = strip_tags($string);
    $words = explode(' ', strip_tags($string));
    $return = trim(implode(' ', array_slice($words, 0, $word_limit)));
    if(strlen($return) < strlen($string)){
        $return .= '...';
    }
    return $return;
}

function is_current_page($route)
{
    if(URL::current() == URL::route($route))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function image_fix_orientation($filename)
{
	try
	{
		$exif = exif_read_data($filename);
		if (!empty($exif['Orientation']))
		{
			$image = imagecreatefromjpeg($filename);
			switch ($exif['Orientation'])
			{
				case 3:
					$image = imagerotate($image, 180, 0);
					break;

				case 6:
					$image = imagerotate($image, -90, 0);
					break;

				case 8:
					$image = imagerotate($image, 90, 0);
					break;
			}

			imagejpeg($image, $filename, 90);
		}
	}
	catch (Exception $exception){
		
	}

}