<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent;
use Carbon\Carbon;

class Event extends Model implements \MaddHatter\LaravelFullcalendar\IdentifiableEvent
{
    protected $fillable = [
        'name', 'city', 'country', 'place', 'date_start', 'date_fin',
        'description', 'activated', 'type', 'link', 'image'
    ];

    /**
     * Deactivate event
     *
     * @return bool
     */
    public function deactivate()
    {
        return $this->update(['activated' => 0]);
    }

    /**
     * Query activated events
     *
     * @return bool
     */
    public function scopeActivated($query)
    {
        return $query->where('activated',1);
    }

    /**
     * Activate event
     *
     * @return bool
     */
    public function activate()
    {
        return $this->update(['activated' => 1]);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->name;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return true;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return Carbon::parse($this->date_start);
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return Carbon::parse($this->date_fin);
    }
}
